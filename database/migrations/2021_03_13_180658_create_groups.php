<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name');

            $table->boolean('can_login')->default(true);
            $table->boolean('can_login_admin')->default(false);
        });


        $groups = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "seeders" . DIRECTORY_SEPARATOR . "groups.json");

        if ($groups === false) {
            error_log('Something bad happened and couldn\'t install default groups');
            die();
        }

        App\Models\Group::insert(json_decode($groups, true));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
