<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupPolicies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_policies', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('path')->default('')->unique(); # underscore notation format (eg: system_power-options)
            $table->string('description')->nullable()->default(null); # Markdown format
            $table->enum('type', ['string', 'bool', 'int', 'float', 'array']); # Types that a group policy can assume
            $table->string('value');
        });

        $groupPolicies = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "seeders" . DIRECTORY_SEPARATOR . "group-policies.json");

        if ($groupPolicies === false) {
            error_log('Something bad happened and couldn\'t install default Group Policies');
            die();
        }

        App\Models\GroupPolicy::insert(json_decode($groupPolicies, true));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_policies');
    }
}
