<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesMetadataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files_metadata', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('path');
            $table->string('title');
            $table->timestamp('release_date')->nullable();
            $table->binary('poster')->nullable();
            $table->json('directors')->nullable();
            $table->json('actors')->nullable();
            $table->json('companies')->nullable();
            $table->json('genres')->nullable();
            $table->json('links')->nullable();
            $table->string('description')->nullable();
            $table->string('language')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files_metadata');
    }
}
