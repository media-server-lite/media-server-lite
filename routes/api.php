<?php

use App\Http\Controllers\AppController;
use App\Http\Controllers\CollectionsController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\FileMetaDataController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\OOBEController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ResumeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('info', [AppController::class, 'info']);

Route::middleware(['first-run'])->group(function () {
    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::prefix('/user')->group(function () {
        Route::get('/loggable', [UserController::class, 'getLoggable']);

        Route::post('/login', [UserController::class, 'login']);

        Route::middleware(['jwt'])->group(function () {
            Route::put('/basic', [UserController::class, 'basic']);
            Route::post('/basic/profile-picture', [UserController::class, 'uploadProfilePicture']);
            Route::put('/password', [UserController::class, 'password']);
            Route::delete('/password', [UserController::class, 'removePassword']);
        });
    });

    Route::prefix('/file')->middleware('jwt')->group(function () {
        Route::prefix('/favorite')->group(function () {
            Route::put('', [FileController::class, 'favorite']);
        });
    });

    Route::prefix('/history')->middleware('jwt')->group(function () {
        Route::delete('', [HistoryController::class, 'clearAll']);
        Route::get('sort/{orderBy}', [HistoryController::class, 'orderBy']);
    });

    Route::prefix('/collections')->middleware('jwt')->group(function () {
        Route::get('', [CollectionsController::class, 'getByUserId']);
        Route::post('', [CollectionsController::class, 'new']);
        Route::patch('{id}', [CollectionsController::class, 'rename'])->where('id', '[0-9]+');
        Route::delete('{id}', [CollectionsController::class, 'delete'])->where('id', '[0-9]+');

        Route::put('pair', [CollectionsController::class, 'pair']);
    });

    Route::prefix('/resumes')->middleware('jwt')->group(function () {
        Route::patch('', [ResumeController::class, 'patch']);
        Route::delete('', [ResumeController::class, 'delete']);
    });

    Route::prefix('/file-metadata')->middleware('jwt')->group(function () {
        Route::get('', [FileMetaDataController::class, 'get']);
        Route::patch('', [FileMetaDataController::class, 'patch']);
        Route::delete('', [FileMetaDataController::class, 'delete']);
        Route::post('poster', [FileMetaDataController::class, 'uploadPoster']);
        Route::post('scrap', [FileMetaDataController::class, 'scrap']);
    });
});

Route::prefix('welcome')->middleware(['first-run'])->group(function () {
    Route::post('admin', [OOBEController::class, 'createAdmin']);

    Route::get('user', [OOBEController::class, 'getAllUsers']);
    Route::post('user', [OOBEController::class, 'createUser']);
    Route::delete('user/{username}', [OOBEController::class, 'deleteUser']);
    Route::post('user/profile-picture/{username}', [OOBEController::class, 'uploadProfilePicture']);

    Route::patch('app', [OOBEController::class, 'saveCustomization']);
});
