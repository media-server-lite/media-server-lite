<?php

use App\Http\Controllers\CollectionsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FolderViewer;
use App\Http\Controllers\FileViewer;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OOBEController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['first-run'])->group(function () {
    Route::get('/', fn () => config('msl.app_splash') ? view('pages.splash') : redirect()->route('home'));

    Route::get('/home', [HomeController::class, 'view'])->name('home')->middleware('jwt');

    Route::get('/b/{path}', [FolderViewer::class, 'view'])->name('folder-viewer')->where('path', '(.*)')->middleware('jwt');

    Route::get('/f/{path}', [FileViewer::class, 'view'])->name('file-viewer')->where('path', '(.*)')->middleware('jwt');

    Route::get('/collection/{id}', [CollectionsController::class, 'view'])->name('collection-viewer')->where('id', '[0-9]+')->middleware('jwt');

    Route::get('/login', [UserController::class, 'loginPage'])->name('login')->middleware('redirect-if-authenticated');

    Route::get('/logout', [UserController::class, 'logout'])->name('logout')->middleware('jwt');

    Route::get('/settings', [SettingsController::class, 'view'])->name('settings')->middleware('jwt');
});

Route::prefix('welcome')->middleware(['first-run'])->group(function () {
    Route::get('', [OOBEController::class, 'view'])->name('oobe');
});
