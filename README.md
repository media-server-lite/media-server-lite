<img alt="Quality Gate Status" src="https://sonarcloud.io/api/project_badges/measure?project=media-server-lite&metric=alert_status"/>

<img alt="Maintainability Rating" src="https://sonarcloud.io/api/project_badges/measure?project=media-server-lite&metric=sqale_rating"/>

<img alt="Reliability Rating" src="https://sonarcloud.io/api/project_badges/measure?project=media-server-lite&metric=reliability_rating"/>

<img alt="Security Rating" src="https://sonarcloud.io/api/project_badges/measure?project=media-server-lite&metric=security_rating"/>

<img alt="Bugs" src="https://sonarcloud.io/api/project_badges/measure?project=media-server-lite&metric=bugs"/>

<img alt="Vulnerabilities" src="https://sonarcloud.io/api/project_badges/measure?project=media-server-lite&metric=vulnerabilities"/>

<img alt="Pipeline" src="https://img.shields.io/gitlab/pipeline/media-server-lite/media-server-lite/main"/>

# Media Server Lite

[![Media Server Lite on a TV](https://msl.francescosorge.com/assets/images/big-buck-bunny-in-tv.png)](https://msl.francescosorge.com)

## A universal streaming platform

Easely watch videos, listen to music and look at pictures on any device with just a browser

## Download and install

Visit the [official website](msl.francescosorge.com/).


## Develop

### Pre-requisites

- Docker


### Steps

1. Run `sail npm install` and `sail composer install` to install project dependencies

2. Create a valid `.env` file in the root of the project. You can use `.env.example` as an example file

3. Run `sail php artisan key:generate && sail php artisan jwt:secret` to generate a valid application key

4. Run `sail php artisan migrate` to create table structures and populate system tables

5. Run `sail up -d` and `sail npm run dev` to launch backend and frontend with hot reload

6. Map at least one folder in `docker-compose.yml` under the `/var/www/html/public/media/` path

7. Start developing!



## Build

### **Docker production image**

1. Update version in:
    - .env.example
    - sonar-project.properties

2. Run `docker build -t RELEASER/msl:VERSION -f dockerfile/msl-production.dockerfile .` to create a production Docker image with the source code


## Code quality

Media Server Lite assures high code quality standards by enforcing different technologies.


### **Static code analysis**

#### **Psalm**

We use [Psalm](https://psalm.dev/) on the strictest level. To check the whole application, simply run: `sail php ./vendor/bin/psalm --show-info=true --no-cache`.

No errors, warnings or info must appear in the output.

#### **SonarCloud/SonarQube**

We use SonarCloud to improve code quality. You can check the official instance on [SonarCloud](https://sonarcloud.io/dashboard?id=media-server-lite).

If you have a SonarQube instance, you can analyze MSL by following these steps.

1. Configure your `.env` file with `SONAR_HOST_URL` and `SONAR_LOGIN`

2. Run the scanner with `docker compose run --rm sonar-scanner`

The scan will last for several minutes. You will then find the results in your SonarQube dashboard project's page.

### **E2E tests**

We use [Cypress](https://www.cypress.io/) to do end to end tests that allows us to find bugs ahead of release. Currently, we have over 100 tests!

#### **Step 1. Configure media files for E2E tests**

Use official test files by downloading them from the official git repository: `git clone https://gitlab.com/media-server-lite/media-server-lite-tests-media.git cypress/media`

#### **Step 2. Build and run the test container**

Cypress, by default, connects to a Docker container built only for running E2E tests. You can build and bring it up easily by running: `docker compose up --build -d msl-test`

#### **Step 3. Run Cypress tests**

Either:

 - Run in GUI mode: `npm run e2e`
 - Run in CLI mode: `npm run e2e-cli`
