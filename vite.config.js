import { defineConfig } from "vite";

import laravel from 'laravel-vite-plugin'
import { svelte } from '@sveltejs/vite-plugin-svelte';

export default defineConfig({
    server: {
        hmr: {
            host: 'localhost'
        },
    },
    plugins: [
        laravel.default([
            'resources/js/app.js',
            'resources/scss/app.scss'
        ]),
        svelte()
    ],
});
