# Build frontend
FROM node:18.15 AS frontend

RUN mkdir /root/app

WORKDIR /root/app

COPY package*.json .

RUN npm install

COPY ../postcss.config.cjs .
COPY ../vite.config.js .
COPY ../tailwind.config.cjs .
COPY ../resources ./resources

RUN npm run build

RUN npm install --production


# Build PHP and Laravel backend
FROM fsorge/php8.1 AS backend

WORKDIR /var/www/html

COPY ../ .

COPY .env.example .env

RUN adduser --disabled-password --gecos "" msl

RUN touch database/sqlite/database.sqlite && chown msl -R database/sqlite

RUN composer install --no-dev --optimize-autoloader && php artisan key:generate

RUN chown -R msl storage/

RUN php artisan distupgrade && php artisan jwt:secret

RUN php artisan view:cache && php artisan route:cache

RUN mkdir public/media

RUN chown -R msl public/*

COPY --from=frontend /root/app/public /var/www/html/public

USER msl

EXPOSE 80

ENTRYPOINT [ "sh", "-c", "php artisan distupgrade && apachectl -D FOREGROUND" ]
