import { createInertiaApp } from '@inertiajs/svelte'

createInertiaApp({
  resolve: name => {
    const pages = import.meta.glob('./Pages/**/*.svelte', { eager: true })
    return pages[`./Pages/${name}.svelte`]
  },
  setup({ el, App, props }) {
    new App({ target: el, props })
  },
})

/**
 * Parses a length and returns a string in the format h:mm:ss.
 * @param {number} length - integer seconds
 */
export const parseLength = (length) => {
    length = Math.round(length);
    if (length < 60) return `0:${(length + "").padStart(2, "0")}`;

    let minutes = ~~(length / 60);
    const seconds = length % 60;

    if (minutes >= 60) {
        const hours = ~~(length / 3600);
        minutes = minutes - hours * 60;

        return `${hours}:${(minutes + "").padStart(2, "0")}:${(
            seconds + ""
        ).padStart(2, "0")}`;
    } else {
        return `${minutes}:${(seconds + "").padStart(2, "0")}`;
    }
};
