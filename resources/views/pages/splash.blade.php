<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('msl.app_name') }}</title>

    <!-- Styles -->
    <link href="/css/tw.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">

    <meta http-equiv="refresh" content="3; URL='/home'" />
</head>

<body class="antialiased">
    <div
        class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
        <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 p-6 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg text-center">
                <h2 class="text-4xl">{{ config('msl.app_name') }}</h2>
                <p class="my-2 text-gray-500">Version {{ config('app.version') }}</p>

                <p class="fa fa-spinner text-4xl text-gray-500 animate-spin block m-auto"></p>
            </div>
        </div>
    </div>
</body>

</html>
