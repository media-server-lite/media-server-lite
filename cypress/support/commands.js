// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import 'cypress-file-upload';

const { default: axios } = require("axios");

Cypress.Commands.add('suppressForm', (selector) => {
    cy.get(selector).then(form$ => {
        form$.on("submit", e => {
            e.preventDefault();
        });
    });
});

Cypress.Commands.add('checkFavorite', (path) => {
    let favorite = false
    const favoriteSelector = '[data-cy="button favorite"]'

    cy.intercept('PUT', 'api/file/favorite', req => {
        req.reply(res => {
            if (!favorite) {
                res.send({
                    body: {
                        status: 200,
                        message: 'success',
                        data: 'created'
                    }
                })
            } else {
                res.send({
                    body: {
                        status: 200,
                        message: 'success',
                        data: 'removed'
                    }
                })
            }
            favorite = !favorite
        })
    }).as('favorite')

    cy.get(favoriteSelector).click()

    cy.get(favoriteSelector).contains('adding', { matchCase: false })

    cy.wait('@favorite')
    favorite = true

    cy.get(favoriteSelector).contains('remove from favorite', { matchCase: false })

    cy.get(favoriteSelector).click()

    cy.get(favoriteSelector).contains('removing', { matchCase: false })

    cy.wait('@favorite')
    favorite = false

    cy.get(favoriteSelector).contains('add to favorite', { matchCase: false })
})

Cypress.Commands.add('checkItemStructure', (item) => {
    const supported_items_type = ['Folder', 'Video', 'Image', 'Audio', 'Playlist', 'Collection', 'Unknown']

    cy.wrap(item).children('[data-cy=item-name]')
    cy.wrap(item).children('[data-cy=item-icon]')

    cy.wrap(item).children('[data-cy=item-type]').invoke("text").then(text => {
        text = text.trim()
        expect(text).to.contain.oneOf(supported_items_type)

        if (text === 'Image') {
            cy.wrap(item).children('img[data-cy=item-icon]').should('has.attr', 'src')
        } else if (text === 'Video') {
            cy.wrap(item).children().should('has.class', 'fa-film')
        } else if (text === 'Audio') {
            cy.wrap(item).children().should('has.class', 'fa-music')
        } else if (text === 'Playlist') {
            cy.wrap(item).children().should('has.class', 'fa-list-ol')
        } else if (text === 'Unknown') {
            cy.wrap(item).children().should('has.class', 'fa-question')
            cy.wrap(item).children('.text-xs').should('contain', '.')
        }
    })
})

Cypress.Commands.add('clearHistory', () => {
    cy.request('DELETE', 'api/history')
})

cy.firstRun = async () => {
    return (await axios.get(Cypress.config().baseUrl + 'api/info')).data.data.firstRun;
}

cy.elementExists = async (ele) => {
    return new Cypress.Promise((resolve, reject) => {
        cy.get('body').then((body) => {
            if (body.find(ele).length > 0) {
                resolve();
            } else {
                reject();
            }
        });
    });
}

cy.apiLogin = async (username = null, password = null) => {
    const { headers } = await cy.request('POST', 'api/user/login', {
        username: username ?? Cypress.env('user').username,
        password: password ?? Cypress.env('user').password,
    });

    if (headers['set-cookie']) {
        cy.token = headers['set-cookie'][0].split(';').map(c => c.split('=')).find(cookie => {
            return cookie[0] === 'auth-token';
        })[1];
    }
};
