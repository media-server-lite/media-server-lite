describe('Out Of the Box Experience', () => {

    const application = Cypress.env('application')
    const admin = Cypress.env('admin')
    const user = Cypress.env('user')

    before(() => {
        Cypress.config('defaultCommandTimeout', 500);
    });

    it('Shows welcome screen', () => {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get("[data-cy=header]").contains('Welcome')
        cy.get('[data-cy="button start"]')
    })

    it('[Create administrator account] Show', () => {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy=header]').contains('administrator')
    })

    it('[Create administrator account] Can go back', () => {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()

        cy.get('[data-cy="button go-back"]').click()
        cy.get("[data-cy=header]").contains('Welcome')
    })

    it('[Create administrator account] Submit no name', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()

        cy.elementExists('[data-cy="alert warning"]')
            .then(() => {
                this.skip();
            })
            .catch(() => {
                // Do nothing (continues the test)
            })

        cy.get('[data-cy="button create"]').click()
        cy.get('[data-cy="alert error"]').contains('Name').contains('empty')
    })

    it('[Create administrator account] Submit no username', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()

        cy.elementExists('[data-cy="alert warning"]')
            .then(() => {
                this.skip();
            })
            .catch(() => {
                // Do nothing (continues the test)
            })

        cy.get('[data-cy=name]').type(admin.name)

        cy.get('[data-cy="button create"]').click()
        cy.get('[data-cy="alert error"]').contains('Username').contains('empty')
    })

    it('[Create administrator account] Submit no password', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()

        cy.elementExists('[data-cy="alert warning"]')
            .then(() => {
                this.skip();
            })
            .catch(() => {
                // Do nothing (continues the test)
            })

        cy.get('[data-cy=name]').type(admin.name)
        cy.get('[data-cy=username]').type(admin.username)

        cy.get('[data-cy="button create"]').click()
        cy.get('[data-cy="alert error"]').contains('Password').contains('empty')
    })

    it('[Create administrator account] Submit password mismatch', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()

        cy.elementExists('[data-cy="alert warning"]')
            .then(() => {
                this.skip();
            })
            .catch(() => {
                // Do nothing (continues the test)
            })

        cy.get('[data-cy=name]').type(admin.name)
        cy.get('[data-cy=username]').type(admin.username)
        cy.get('[data-cy=password]').type(admin.password)

        cy.get('[data-cy="button create"]').click()
        cy.get('[data-cy="alert error"]').contains('passwords').contains('same')

        cy.get('[data-cy=passwordRepeat]').type(admin.passwordWrong)
        cy.get('[data-cy="button create"]').click()
        cy.get('[data-cy="alert error"]').contains('passwords').contains('same')
    })

    it('[Create administrator account] Submit valid', function () {
        if (!cy.firstRunResult) return

        cy.intercept('POST', '/api/welcome/admin').as('createAdmin')

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()

        cy.elementExists('[data-cy="alert warning"]')
            .then(() => {
                this.skip();
            })
            .catch(() => {
                // Do nothing (continues the test)
            })

        cy.get('[data-cy=name]').type(admin.name)
        cy.get('[data-cy=username]').type(admin.username)
        cy.get('[data-cy=password]').type(admin.password)
        cy.get('[data-cy=passwordRepeat]').type(admin.password)
        cy.get('[data-cy="button create"]').click()
        cy.get('[data-cy="button create"]').contains('Creating')

        cy.wait('@createAdmin')
        cy.get("[data-cy=header]").contains('User')
    })

    it('[Create administrator account] Already exists', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()

        cy.get('[data-cy="alert warning"]')

        cy.get('[data-cy="button next"]').click()
        cy.get("[data-cy=header]").contains('User')
    })

    it('[Create user account] Show', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.get("[data-cy=header]").contains('User')
    })

    it('[Create user account] Submit no name', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.elementExists('[data-cy="user entry"]')
            .then(() => {
                this.skip();
            })
            .catch(() => {
                // Do nothing (continues the test)
            })

        cy.get('[data-cy="button create"]').click()
        cy.get('[data-cy="alert error"]').contains('Name').contains('empty')
    })

    it('[Create user account] Submit no username', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.elementExists('[data-cy="user entry"]')
            .then(() => {
                this.skip();
            })
            .catch(() => {
                // Do nothing (continues the test)
            })

        cy.get('[data-cy=name]').type(user.name)

        cy.get('[data-cy="button create"]').click()
        cy.get('[data-cy="alert error"]').contains('Username').contains('empty')
    })

    it('[Create user account] Submit password mismatch', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.elementExists('[data-cy="user entry"]')
            .then(() => {
                this.skip();
            })
            .catch(() => {
                // Do nothing (continues the test)
            })

        cy.get('[data-cy=name]').type(user.name)
        cy.get('[data-cy=username]').type(user.username)
        cy.get('[data-cy=password]').type(user.password)

        cy.get('[data-cy="button create"]').click()
        cy.get('[data-cy="alert error"]').contains('passwords').contains('same')

        cy.get('[data-cy=passwordRepeat]').type(user.passwordWrong)
        cy.get('[data-cy="button create"]').click()
        cy.get('[data-cy="alert error"]').contains('passwords').contains('same')
    })

    it('[Create user account] Submit valid', function () {
        if (!cy.firstRunResult) return

        cy.intercept('POST', '/api/welcome/user').as('createUser')

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.elementExists('[data-cy="user entry"]')
            .then(() => {
                this.skip();
            })
            .catch(() => {
                // Do nothing (continues the test)
            })

        cy.get('[data-cy=name]').type(user.name)
        cy.get('[data-cy=username]').type(user.username)
        cy.get('[data-cy=password]').type(user.password)
        cy.get('[data-cy=passwordRepeat]').type(user.password)
        cy.get('[data-cy="button create"]').click()
        cy.get('[data-cy="button create"]').contains('Creating')

        cy.wait('@createUser')
        cy.get("[data-cy=header]").contains('Users')
    })

    it('[Create user account] Can create new', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()
        cy.get('[data-cy="button new"]').click()

        cy.get("[data-cy=header]").contains('User creation')
    })

    it('[Create user account] Can go back from new', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()
        cy.get('[data-cy="button new"]').click()
        cy.get('[data-cy="button cancel"]').click()

        cy.get("[data-cy=header]").contains('Users')
    })

    it('[Users] List', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.get("[data-cy=header]").contains('Users')
        cy.get(".user").should('have.length.gt', 0)
    })

    it('[Users] Delete one', function () {
        if (!cy.firstRunResult) return

        cy.intercept('DELETE', '/api/welcome/user/**', { data: { status: 'deleted' } }).as('deleteUser')

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.get("[data-cy=header]").contains('Users')
        cy.get("[data-cy='button delete']").first().click()

        cy.wait('@deleteUser')

        cy.get("[data-cy=header]").contains('User creation')
    })

    it('[Customization] Show', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.get("[data-cy=header]").contains('customize')
    })

    it('[Customization] Submit no application name', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.get('[data-cy=appName').clear()

        cy.get('[data-cy="button save"]').click()
        cy.get('[data-cy="alert error"]').contains('Application name').contains('empty')
    })

    it('[Customization] Add hidden entry', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.get('[data-cy="hidden-entry input"]').then((b) => {
            const length = { before: b.length }
            cy.get('[data-cy="button add-pattern"]').click()

            cy.get('[data-cy="hidden-entry input"]').then((a) => {
                length.after = a.length

                expect(length.before).to.be.eq(length.after - 1)
            })
        })
    })

    it('[Customization] Remove hidden entry', function () {
        if (!cy.firstRunResult) return

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.get('[data-cy="hidden-entry input"]').then((b) => {
            const length = { before: b.length }
            cy.get('[data-cy="button delete-pattern"]').first().click()

            cy.get('[data-cy="hidden-entry input"]').then((a) => {
                length.after = a.length

                expect(length.before).to.be.eq(length.after + 1)
            })
        })
    })

    it('[Customization] Change application name, save and go to app', function () {
        if (!cy.firstRunResult) return

        cy.intercept('PATCH', '/api/welcome/app').as('customize')
        cy.intercept('GET', '/logout').as('logout')

        cy.visit('welcome')
        cy.get('[data-cy="button start"]').click()
        cy.get('[data-cy="button next"]').click()
        cy.get('[data-cy="button next"]').click()

        cy.get('[data-cy="appName"]').clear().type(application.name)

        cy.get('[data-cy="button save"]').click()
        cy.get('[data-cy="button save"]').contains('Customizing')
        cy.wait('@customize')

        cy.get('[data-cy="header"]').contains('That\'s it')
        cy.get('[data-cy="button goToApp"]').click()

        cy.wait('@logout')

        cy.url().should('include', 'login')
    })

})
