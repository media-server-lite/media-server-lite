describe('[Settings] Password', () => {

    const user = Cypress.env('user')
    const newPassword = Cypress.env('user').newPassword ?? `${user.password}6`

    before(() => {
        if (cy.firstRunResult) return

        cy.apiLogin()
    })

    beforeEach(() => {
        if (cy.firstRunResult) return

        if (cy.token) {
            cy.setCookie('auth-token', cy.token)
        }

        cy.visit('settings')
    })

    it('Can click button and panel opens', () => {
        if (cy.firstRunResult) return

        openPanel()
    })

    it('Submit without current password', () => {
        if (cy.firstRunResult) return

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click() // Clicks update button

        cy.get('[data-cy=settings-panel] [data-cy="alert error"]').contains('Current password cannot be empty')
    })

    it('Submit without new password', () => {
        if (cy.firstRunResult) return

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=current-password]').type(user.password)

        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click() // Clicks update button

        cy.get('[data-cy=settings-panel] [data-cy="alert error"]').contains('New password cannot be empty')
    })

    it('Submit without confirming password', () => {
        if (cy.firstRunResult) return

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=current-password]').type(user.password)
        cy.get('[data-cy=settings-panel] [data-cy=new-password]').type(newPassword)

        // cy.intercept('PUT', '/api/user/password').as('update') // intercepts REST request
        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click() // Clicks update button

        cy.get('[data-cy=settings-panel] [data-cy="alert error"]').contains('The two passwords are not the same')
    })

    it('Submit mismatch password', () => {
        if (cy.firstRunResult) return

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=current-password]').type(user.password)
        cy.get('[data-cy=settings-panel] [data-cy=new-password]').type(newPassword)
        cy.get('[data-cy=settings-panel] [data-cy=new-password-repeat]').type(`${user.password}b`)

        // cy.intercept('PUT', '/api/user/password').as('update') // intercepts REST request
        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click() // Clicks update button

        cy.get('[data-cy=settings-panel] [data-cy="alert error"]').contains('The two passwords are not the same')
    })

    it('Submit wrong current password', () => {
        if (cy.firstRunResult) return

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=current-password]').type(`${user.password}a`)
        cy.get('[data-cy=settings-panel] [data-cy=new-password]').type(newPassword)
        cy.get('[data-cy=settings-panel] [data-cy=new-password-repeat]').type(newPassword)

        cy.intercept('PUT', '/api/user/password').as('update') // intercepts REST request
        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click().contains('Updating') // Clicks update button
        cy.wait('@update')

        cy.get('[data-cy=settings-panel] [data-cy="alert error"]').contains('The password you provided is not correct')
    })

    it('Correctly change password', () => {
        if (cy.firstRunResult) return

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=current-password]').type(user.password)
        cy.get('[data-cy=settings-panel] [data-cy=new-password]').type(newPassword)
        cy.get('[data-cy=settings-panel] [data-cy=new-password-repeat]').type(newPassword)

        cy.intercept('PUT', '/api/user/password').as('update') // intercepts REST request
        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click().contains('Updating') // Clicks update button
        cy.wait('@update')

        cy.get('[data-cy=settings-panel] [data-cy="alert success"]').contains('Password updated')

        cy.get('[data-cy=settings-panel] [data-cy=current-password]').should('have.value', '')
        cy.get('[data-cy=settings-panel] [data-cy=new-password]').should('have.value', '')
        cy.get('[data-cy=settings-panel] [data-cy=new-password-repeat]').should('have.value', '')
    })

    it('Can\'t login with old password', () => {
        if (cy.firstRunResult) return
        
        cy.clearCookie('auth-token')
        cy.intercept('GET', '/api/user/loggable').as('getLoggable')
        cy.reload()

        cy.wait('@getLoggable')

        cy.get('[data-cy=password]').type(user.password)

        cy.suppressForm("form")
        cy.intercept('POST', '/api/user/login').as('login')
        cy.get('[data-cy=login-button]').click()
        cy.get('[data-cy=loading-indicator]')
        cy.wait('@login')

        cy.get('[data-cy=invalid-pin]')
    })

    it('Can login with new password', () => {
        if (cy.firstRunResult) return

        cy.clearCookie('auth-token')
        cy.intercept('GET', '/api/user/loggable').as('getLoggable')
        cy.reload()

        cy.wait('@getLoggable')

        cy.suppressForm("form")
        cy.intercept('POST', '/api/user/login').as('login')

        cy.get('[data-cy=password]').type(newPassword)
        cy.get('[data-cy=login-button]').click()
        cy.get('[data-cy=loading-indicator]')

        cy.intercept('GET', 'settings').as('settings')

        cy.wait('@settings').then(settings => {
            cy.url().should('include', 'settings')
            expect(settings.response.statusCode).to.be.equal(200)
        })
    })

    it('Can\'t remove remove password without current one', () => {
        if (cy.firstRunResult) return

        cy.apiLogin(null, newPassword)

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy="button remove"]').click() // Clicks remove button
        cy.get('[data-cy=settings-panel] [data-cy="alert error"]').contains('Current password cannot be empty')
    })

    it('Can\'t remove remove password if current one is wrong', () => {
        if (cy.firstRunResult) return

        cy.apiLogin(null, newPassword)

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=current-password]').type(user.password)
        cy.get('[data-cy=settings-panel] [data-cy="button remove"]').click() // Clicks remove button
        cy.get('[data-cy=settings-panel] [data-cy="alert error"]').contains('The password you provided is not correct')
    })

    it('Correctly remove password', () => {
        if (cy.firstRunResult) return

        cy.apiLogin(null, newPassword)

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=current-password]').type(newPassword)

        cy.intercept('DELETE', '/api/user/password').as('delete') // intercepts REST request
        cy.get('[data-cy=settings-panel] [data-cy="button remove"]').click().contains('Removing') // Clicks remove button
        cy.wait('@delete')

        cy.get('[data-cy=settings-panel] [data-cy="alert success"]').contains('Password removed')

        cy.get('[data-cy=settings-panel] [data-cy=current-password]').should('not.exist')
    })

    it('Set password from unprotected account', () => {
        if (cy.firstRunResult) return

        cy.apiLogin(null, undefined)

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=new-password]').type(user.password)
        cy.get('[data-cy=settings-panel] [data-cy=new-password-repeat]').type(user.password)

        cy.intercept('PUT', '/api/user/password').as('set') // intercepts REST request
        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click().contains('Updating') // Clicks remove button
        cy.wait('@set')

        cy.get('[data-cy=settings-panel] [data-cy="alert success"]').contains('Password updated')

        cy.get('[data-cy=settings-panel] [data-cy=current-password]')
        cy.get('[data-cy=settings-panel] [data-cy="button remove"]')
    })

    const openPanel = () => {
        cy.get('[data-cy="settings-panel-button security-and-password"]').click()
        cy.get('[data-cy=settings-panel]')
    }

})
