describe('[Settings] User account', () => {

    before(() => {
        if (cy.firstRunResult) return

        cy.apiLogin()
    })

    beforeEach(() => {
        if (cy.firstRunResult) return

        cy.setCookie('auth-token', cy.token)

        cy.visit('settings')
    })

    it('[user connected] Can view page', () => {
        if (cy.firstRunResult) return

        cy.url().should('not.include', 'login').and('include', 'settings')
        cy.get('[data-cy=header]').contains('Settings')
    })

    it('[no user] Is redirected to login', () => {
        if (cy.firstRunResult) return

        cy.clearCookie('auth-token')

        cy.reload()

        cy.url().should('include', 'login').and('include', 'redirect=settings')
    })

    it('Structure is correct', () => {
        if (cy.firstRunResult) return

        cy.get('[data-cy=header]')
        cy.get('[data-cy^=settings-panel-button]')
        cy.get('[data-cy=settings-panel]').should('not.exist')
        cy.get('[data-cy=settings-begin-label]')
    })

    it('Can click button and panel opens', () => {
        if (cy.firstRunResult) return

        openPanel()
    })

    it('Change name', () => {
        if (cy.firstRunResult) return

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=name]').type('a') // Adds an 'a' at the end of current name

        cy.intercept('PUT', '/api/user/basic').as('update') // intercepts REST request
        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click().contains('Saving') // Click button and checks that it contains the loading text
        cy.wait('@update') // Waits for REST to complete

        cy.get('[data-cy=settings-panel] [data-cy="alert success"]').contains('User updated') // We check that it contains a valid message

        cy.reload() // Reloads the page to check that the name is correctly saved
        openPanel()

        resetName()
    })

    it('Upload profile picture', () => {
        if (cy.firstRunResult) return

        openPanel()

        uploadProfilePicture()
    })

    it('Clear profile picture but selects new one', () => {
        if (cy.firstRunResult) return

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=clear-profile-picture]').click()
        cy.get('[data-cy=settings-panel] [data-cy=clear-profile-picture-info]')

        cy.get('[data-cy=settings-panel] [data-cy=upload-profile-picture]').attachFile('image-2.png') // Attach a new file
        cy.get('[data-cy=settings-panel] [data-cy=clear-profile-picture-info]').should('not.exist') // The alert should be removed

        cy.intercept('PUT', '/api/user/basic').as('update') // intercepts REST request
        cy.intercept('POST', '/api/user/basic/profile-picture').as('uploadProfilePicture') // intercepts upload profile picture REST request

        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click() // Clicks update button

        cy.wait('@update')
        cy.wait('@uploadProfilePicture')

        cy.get('[data-cy=settings-panel] [data-cy="alert success"]').contains('User updated') // We check that it contains a valid message

        cy.reload()

        openPanel()
        cy.get('[data-cy=settings-panel] [data-cy=clear-profile-picture]')
    })

    it('Clear profile picture', () => {
        if (cy.firstRunResult) return

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=clear-profile-picture]').click()
        cy.get('[data-cy=settings-panel] [data-cy=clear-profile-picture-info]')

        cy.intercept('PUT', '/api/user/basic').as('update') // intercepts REST request
        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click() // Clicks update button
        cy.wait('@update')

        cy.get('[data-cy=settings-panel] [data-cy="alert success"]').contains('User updated') // We check that it contains a valid message

        cy.reload()

        openPanel()
        cy.get('[data-cy=settings-panel] [data-cy=clear-profile-picture-info]').should('not.exist')
    })

    it('Change name and upload profile picture', () => {
        if (cy.firstRunResult) return

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=name]').type('a') // Adds an 'a' at the end of current name

        uploadProfilePicture()

        cy.reload() // Reloads the page to check that the name is correctly saved
        openPanel()

        resetName()
    })

    it('Change name and clear profile picture', () => {
        if (cy.firstRunResult) return

        openPanel()

        cy.get('[data-cy=settings-panel] [data-cy=name]').type('a') // Adds an 'a' at the end of current name
        cy.get('[data-cy=settings-panel] [data-cy=clear-profile-picture]').click() // Removes profile picture

        cy.intercept('PUT', '/api/user/basic').as('update') // intercepts REST request
        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click() // Clicks update button
        cy.wait('@update')

        cy.reload() // Reloads the page to check that the name is correctly saved
        openPanel()

        resetName()
    })

    const openPanel = () => {
        cy.get('[data-cy="settings-panel-button user-account"]').click()
        cy.get('[data-cy=settings-panel]')
    }

    const uploadProfilePicture = () => {
        cy.get('[data-cy=settings-panel] [data-cy=upload-profile-picture]').attachFile('image.png')

        cy.intercept('PUT', '/api/user/basic').as('update') // intercepts REST request
        cy.intercept('POST', '/api/user/basic/profile-picture').as('uploadProfilePicture') // intercepts upload profile picture REST request

        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click() // Clicks update button

        cy.wait('@update')
        cy.wait('@uploadProfilePicture')

        cy.get('[data-cy=settings-panel] [data-cy="alert success"]').contains('User updated') // We check that it contains a valid message
    }

    const resetName = () => {
        cy.get('[data-cy=settings-panel] [data-cy=name]').should('contain.value', 'a') // We check that the name contains the latest character

        cy.get('[data-cy=settings-panel] [data-cy=name]').type('{backspace}') // We remove the latest character
        cy.get('[data-cy=settings-panel] [data-cy="button update"]').click() // And update again so that it gets resetted to default name
        cy.wait('@update')
    }

})
