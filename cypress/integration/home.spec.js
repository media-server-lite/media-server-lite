describe('Home page', () => {
    before(() => {
        if (cy.firstRunResult) return

        cy.apiLogin()
    })

    beforeEach(() => {
        if (cy.firstRunResult) return

        cy.setCookie('auth-token', cy.token)

        cy.visit('home')
    })

    it('[user connected] Can view homepage', () => {
        if (cy.firstRunResult) return

        cy.url().should('not.include', 'login').and('include', 'home')
        cy.get('[data-cy=header]').contains('Home')
    })

    it('[no user] Is redirected to login', () => {
        if (cy.firstRunResult) return

        cy.clearCookie('auth-token')

        cy.visit('home')

        cy.url().should('include', 'login').and('include', 'redirect=home')
    })

    it('Structure is correct', () => {
        if (cy.firstRunResult) return

        cy.get('[data-cy=header]')
        cy.get('[data-cy=user-avatar]').should('have.attr', 'src')
        cy.get('[data-cy=user-name]').should('not.be.empty')
        cy.get('[data-cy=welcome-text]').contains('Nice to see you again 😊')
        cy.get('[data-cy=my-files]').children()
        cy.get('[data-cy=favorites]')
        cy.get('[data-cy=history]')
    })

    it('Can show history', () => {
        if (cy.firstRunResult) return

        const image = Cypress.env('file-viewer').pathImage
        cy.visit(`f/${image}`)
        cy.visit('home')

        cy.get('[data-cy="history"] [data-cy="title-view"] [data-cy="entry-item"]').each(item => {
            cy.checkItemStructure(item)
        })

        cy.clearHistory()
    })

    it('Can clear history', () => {
        if (cy.firstRunResult) return

        cy.intercept('DELETE', 'api/history').as('clearHistory')

        const image = Cypress.env('file-viewer').pathImage
        cy.visit(`f/${image}`)
        cy.visit('home')

        cy.get('[data-cy="button clear-history"]').click()
        cy.get('[data-cy="button clear-history"]').contains('clearing', { matchCase: false })

        cy.wait('@clearHistory')

        cy.get('[data-cy="button clear-history"]').should('not.exist')
        cy.get('[data-cy="history"] [data-cy="title-view"] [data-cy="entry-item"]').should('not.exist')
    })

    it('Can navigate to a folder', () => {
        if (cy.firstRunResult) return

        cy.intercept('GET', 'b/**').as('folderViewer')

        cy.get('[data-cy=my-files] [tabindex="1"]').click()

        cy.wait('@folderViewer').then(folderViewer => {
            const statusCode = folderViewer.response.statusCode

            expect(statusCode).to.be.equal(200)
        })
    })
})
