describe('Collections', () => {

    const image = Cypress.env('file-viewer').pathImage
    const name = Cypress.env('collection-viewer')?.name ?? 'Plants'
    const renamed = Cypress.env('collection-viewer')?.renamed ?? 'Hello'

    before(() => {
        if (cy.firstRunResult) return

        cy.apiLogin()
    })

    beforeEach(() => {
        if (cy.firstRunResult) return

        cy.setCookie('auth-token', cy.token)
    })

    it('Create a collection', () => {
        if (cy.firstRunResult) return

        cy.intercept('GET', '/api/collections?path=/**').as('collections')
        cy.intercept('POST', '/api/collections').as('createCollection')

        cy.visit(`f/${image}`)

        cy.get('[data-cy="button collections"]').click()

        cy.wait('@collections')

        cy.get('[data-cy="button go-to-new-collection"]').click()

        cy.get('[data-cy="collections new-input"]').type(name)

        cy.get('[data-cy="button create-new-collection"]').click()
        cy.wait('@createCollection')
        cy.get('[data-cy="alert error"]').should('not.exist')
    })

    it('Create a collection with existing name', () => {
        if (cy.firstRunResult) return

        cy.intercept('GET', '/api/collections?path=/**').as('collections')
        cy.intercept('POST', '/api/collections').as('createCollection')

        cy.visit(`f/${image}`)

        cy.get('[data-cy="button collections"]').click()

        cy.wait('@collections')

        cy.get('[data-cy="button go-to-new-collection"]').click()

        cy.get('[data-cy="collections new-input"]').type(name)

        cy.get('[data-cy="button create-new-collection"]').click()
        cy.wait('@createCollection')
        cy.get('[data-cy="alert error"]').should('exist').and('contain', 'already exist', { matchCase: false })
    })

    it('Add to collection', () => {
        if (cy.firstRunResult) return

        cy.intercept('GET', '/api/collections?path=/**').as('collections')
        cy.intercept('PUT', '/api/collections/pair').as('addRemovePair')

        cy.visit(`f/${image}`)

        cy.get('[data-cy="button collections"]').click()

        cy.wait('@collections')
        cy.get(`[data-cy="collections entry"]:contains("${name}")`).find('[data-cy="button add-to-collection"]').should('contain', 'Add').click()
        cy.get('[data-cy="button add-to-collection"]').contains('Adding...')
        cy.wait('@addRemovePair')
    })

    it('Collection shows items', () => {
        if (cy.firstRunResult) return

        cy.visit('home')

        cy.get(`[data-cy=collections] [data-cy=entry-item]:contains("${name}")`).click()

        cy.get('[data-cy=title-view]').children()
    })

    it('Remove from collection', () => {
        if (cy.firstRunResult) return

        cy.intercept('GET', '/api/collections?path=/**').as('collections')
        cy.intercept('PUT', '/api/collections/pair').as('addRemovePair')

        cy.visit(`f/${image}`)

        cy.get('[data-cy="button collections"]').click()

        cy.wait('@collections')
        cy.get(`[data-cy="collections entry"]:contains("${name}")`).find('[data-cy="button add-to-collection"]').should('contain', 'Remove').click()
        cy.get('[data-cy="button add-to-collection"]').contains('Removing...')
        cy.wait('@addRemovePair')
    })

    it('Collection can be renamed', () => {
        if (cy.firstRunResult) return

        cy.intercept('PATCH', '/api/collections/**').as('rename')

        cy.visit('home')

        cy.get(`[data-cy=collections] [data-cy=entry-item]:contains("${name}")`).click()

        cy.get('[data-cy="button rename"]').click()
        cy.get('[data-cy="collection rename"]').clear().type(renamed)
        cy.get('[data-cy="button do-rename"]').click()
        cy.get('[data-cy="button do-rename"]').contains('Renaming...')
        cy.wait('@rename')
    })

    it('Collection cannot be renamed with same or existing name', () => {
        if (cy.firstRunResult) return

        cy.intercept('PATCH', '/api/collections/**').as('rename')

        cy.visit('home')

        cy.get(`[data-cy=collections] [data-cy=entry-item]:contains("${renamed}")`).click()

        cy.get('[data-cy="button rename"]').click()
        cy.get('[data-cy="collection rename"]').clear().type(renamed)
        cy.get('[data-cy="button do-rename"]').click()
        cy.get('[data-cy="button do-rename"]').contains('Renaming...')
        cy.wait('@rename')
        cy.get('[data-cy="alert error"]').should('exist').and('contain', 'already exist', { matchCase: false })
    })

    it('Remove collection', () => {
        if (cy.firstRunResult) return

        cy.intercept('DELETE', '/api/collections/**').as('deleteCollection')
        cy.intercept('GET', 'home').as('home')

        cy.visit('home')

        cy.get(`[data-cy=collections] [data-cy=entry-item]:contains("${renamed}")`).click()

        cy.get('[data-cy="button delete"]').click()
        cy.get('[data-cy="button delete-collection"]').click()
        cy.get('[data-cy="button delete-collection"]').contains('Deleting...')
        cy.wait('@deleteCollection')

        cy.wait('@home')
        cy.url().should('include', 'home')
    })

    it('Home page doesn\'t have any collection', () => {
        if (cy.firstRunResult) return

        cy.visit('home')

        cy.get(`[data-cy=collections] [data-cy=entry-item]:contains("${renamed}")`).should('not.exist')
    })

    // it('[user connected] Can view file', () => {
    //     cy.visit(`f/${image}`)

    //     cy.url().should('not.include', 'login').and('include', `f/${image}`)
    // })

    // it('[no user] Is redirected to login', () => {
    //     cy.clearCookie('auth-token')

    //     cy.visit(`f/${image}`)

    //     cy.url().should('include', 'login').and('include', `redirect=${encodeURIComponent(`f/${image}`)}`)
    // })

    // it('Header is correct', () => {
    //     cy.visit(`f/${image}`)

    //     let fileName = path.basename(image, path.extname(image))

    //     cy.get('[data-cy=header]').contains(new RegExp(`^${fileName}$`))
    // })

    // it('Breadcrumb is correct', () => {
    //     cy.visit(`f/${image}`)

    //     let fileName = path.basename(image, path.extname(image))

    //     cy.get('[data-cy=breadcrumb]').should('contain', 'Home').and('contain', fileName)
    // })


})
