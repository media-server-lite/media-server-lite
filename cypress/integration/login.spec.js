describe('Login', () => {
    const twoUsers = {
        status: 200, message: "success", data: [
            {
                has_password: true,
                id: 2,
                name: "User",
                profile_picture: "/images/avatar_black.png",
                profile_picture_themed: "/images/avatar_white.png",
                username: "user"
            },
            {
                has_password: false,
                id: 3,
                name: "Another fake user",
                profile_picture: "/images/avatar_black.png",
                profile_picture_themed: "/images/avatar_white.png",
                username: "another-user"
            }
        ]
    };

    const oneUser = {
        status: 200, message: "success", data: [
            {
                has_password: true,
                id: 2,
                name: "User",
                profile_picture: "/images/avatar_black.png",
                profile_picture_themed: "/images/avatar_white.png",
                username: "user"
            }
        ]
    };

    const user = Cypress.env('user')

    it('Correctly logins with one user', () => {
        if (cy.firstRunResult) return

        cy.intercept('GET', '/api/user/loggable', {
            body: oneUser
        }).as('getLoggable')

        cy.visit('login')

        cy.url().should('include', 'login')

        cy.wait('@getLoggable').then(loggable => {
            const body = loggable.response.body

            expect(body.message).to.be.equal('success')

            if (body.data.length === 0) {
                Cypress.log({
                    name: 'No user found',
                    message: 'Cannot find any loggable users. This must not happen!'
                })

                expect(true).to.be.false
            }

            cy.suppressForm("form")
            cy.intercept('POST', '/api/user/login').as('login')

            cy.get('[data-cy=selected-user-name]').contains(body.data[0].name)
            cy.get('[data-cy=password]').type(user.password)
            cy.get('[data-cy=login-button]').click()
            cy.get('[data-cy=loading-indicator]')

            cy.intercept('GET', 'home').as('home')

            cy.wait('@home').then(home => {
                cy.url().should('include', 'home')
                expect(home.response.statusCode).to.be.equal(200)
            })
        })
    })

    it('Correctly selects a user', () => {
        if (cy.firstRunResult) return

        cy.intercept('GET', '/api/user/loggable', {
            body: twoUsers
        }).as('getLoggable')

        cy.visit('login')

        cy.wait('@getLoggable').then(loggable => {
            const body = loggable.response.body

            expect(body.message).to.be.equal('success')

            cy.contains('Who are you? 😊')
            cy.get('[data-cy=users]').children().should('have.length', 2)
            cy.get('[tabindex="1"]').click()
            cy.focused().should('have.attr', 'data-cy', 'password')

            cy.get('[data-cy=go-back]').click()
            cy.get('[tabindex="2"]').click()
            cy.get('[data-cy=loading-indicator]')

            cy.suppressForm("form")
            cy.intercept('POST', '/api/user/login').as('login')
        })
    })

    it('Try login with wrong password', () => {
        if (cy.firstRunResult) return

        cy.intercept('GET', '/api/user/loggable', {
            body: oneUser
        }).as('getLoggable')

        cy.visit('login')

        cy.wait('@getLoggable').then(loggable => {
            cy.suppressForm("form")
            cy.intercept('POST', '/api/user/login').as('login')

            cy.get('[data-cy=password]').type(user.passwordWrong)
            cy.get('[data-cy=login-button]').click()

            cy.wait('@login')
            cy.get('[data-cy=invalid-pin]')
        })
    })

    it('Correctly display users', () => {
        if (cy.firstRunResult) return

        cy.intercept('GET', '/api/user/loggable', {
            body: twoUsers
        }).as('getLoggable')

        cy.visit('login')

        cy.url().should('include', 'login')

        cy.wait('@getLoggable').then((interception) => {
            const body = interception.response.body

            expect(body.message).to.be.equal('success')

            cy.contains('Who are you? 😊')
            cy.get('[data-cy=users]').children()
        })
    })
})
