describe('Folder viewer', () => {
    const folder = Cypress.env('folder-viewer').path

    before(() => {
        if (cy.firstRunResult) return

        cy.apiLogin()
    })

    beforeEach(() => {
        if (cy.firstRunResult) return

        cy.setCookie('auth-token', cy.token)

        cy.visit(`b/${folder}`)
    })

    it('[user connected] Can view folder', () => {
        if (cy.firstRunResult) return

        cy.url().should('not.include', 'login').and('include', `b/${folder}`)
    })

    it('[no user] Is redirected to login', () => {
        if (cy.firstRunResult) return

        cy.clearCookie('auth-token')

        cy.visit(`b/${folder}`)

        const encoded = encodeURIComponent(`b/${folder}`)

        cy.url().should('include', 'login').and('include', `redirect=${encoded}`)
    })

    it('Header is correct', () => {
        if (cy.firstRunResult) return

        cy.get('[data-cy=header]').contains(new RegExp(`^${folder}$`))
    })

    it('Breadcrumb is correct', () => {
        if (cy.firstRunResult) return

        cy.get('[data-cy=breadcrumb]').should('contain', 'Home').and('contain', folder)
    })

    it('Favorite button is present', () => {
        if (cy.firstRunResult) return

        cy.get('[data-cy="button favorite"]').should('contain', 'favorite')
    })

    it('Favorite button works', () => {
        if (cy.firstRunResult) return

        cy.checkFavorite(`b/${folder}`)
    })

    it('Check that all item types are displayed correctly', () => {
        if (cy.firstRunResult) return

        cy.get('[data-cy="folder-content"]').children().children().each(item => {
            cy.checkItemStructure(item)
        })
    })
})
