describe('File viewer', () => {
    const path = require('path')

    const image = Cypress.env('file-viewer').pathImage
    const video = Cypress.env('file-viewer').pathVideo
    const videoSubtitled = Cypress.env('file-viewer').pathVideoSubtitled
    const audio = Cypress.env('file-viewer').pathAudio
    const playlist = Cypress.env('file-viewer').pathPlaylist

    before(() => {
        if (cy.firstRunResult) return

        cy.apiLogin();
    })

    beforeEach(() => {
        if (cy.firstRunResult) return

        cy.setCookie('auth-token', cy.token)
    })

    it('[user connected] Can view file', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${image}`)

        cy.url().should('not.include', 'login').and('include', `f/${image}`)
    })

    it('[no user] Is redirected to login', () => {
        if (cy.firstRunResult) return

        cy.clearCookie('auth-token')

        cy.visit(`f/${image}`)

        const encoded = encodeURIComponent(`f/${image}`)

        cy.url().should('include', 'login').and('include', `redirect=${encoded}`)
    })

    it('Header is correct', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${image}`)

        let fileName = path.basename(image, path.extname(image))

        cy.get('[data-cy=header]').contains(new RegExp(`^${fileName}$`))
    })

    it('Breadcrumb is correct', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${image}`)

        let fileName = path.basename(image, path.extname(image))

        cy.get('[data-cy=breadcrumb]').should('contain', 'Home').and('contain', fileName)
    })

    it('Favorite button is present', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${image}`)

        cy.get('[data-cy="button favorite"]').should('contain', 'favorite')
    })

    it('Favorite button works', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${image}`)

        cy.checkFavorite(`f/${image}`)
    })

    it('Video is displayed correctly', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="file-area"]').find('video').children('source').should('have.attr', 'src').and('include', `/${video}`)
        cy.get('[data-cy="file-area"]').find('video').children('source').should('have.attr', 'type').and('include', `video/`)
    })

    it('Video has subtitles', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${videoSubtitled}`)

        cy.get('video track').should('has.attr', 'src').and('length.gt', 0)
        cy.get('video track').should('has.attr', 'label').and('length.gt', 0)
        cy.get('video track').should('has.attr', 'kind').and('be.eq', 'captions')
    })

    it('Audio is displayed correctly', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${audio}`)

        cy.get('[data-cy="file-area"]').find('audio').children('source').should('have.attr', 'src').and('include', `/${audio}`)
        cy.get('[data-cy="file-area"]').find('audio').children('source').should('have.attr', 'type').and('include', `audio/`)
    })

    it('Playlist is displayed correctly', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${playlist}`)

        cy.get('[data-cy="button repeat"]').should('contain', 'repeat')
        cy.get('[data-cy="button shuffle"]').should('contain', 'shuffle')
    })

    it('Playlist table is displayed correctly', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${playlist}`)

        cy.get('[data-cy="file-area"]').find('[data-cy="playlist header"]').children().its('length').then((size) => {
            cy.get('[data-cy="file-area"]').find('[data-cy="playlist entry"]').each(entry => {
                expect(entry.children().length).to.equal(size)
            })
        })
    })

    it('Playlist repeat works', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${playlist}`)

        cy.get('[data-cy="button repeat"]').click()
        cy.get('[data-cy="button repeat"]').contains('Repeat playlist')

        cy.get('[data-cy="button repeat"]').click()
        cy.get('[data-cy="button repeat"]').contains('Repeat track')

        cy.get('[data-cy="button repeat"]').click()
        cy.get('[data-cy="button repeat"]').contains('No repeat')
    })

    it('Playlist shuffle works', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${playlist}`)

        cy.get('[data-cy="button shuffle"]').click()
        cy.get('[data-cy="button shuffle"]').contains('Shuffling')

        cy.get('[data-cy="button shuffle"]').click()
        cy.get('[data-cy="button shuffle"]').contains('No shuffle')
    })

    it('Image is displayed correctly', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${image}`)

        cy.get('[data-cy="file-area"]').find('img').should('have.attr', 'src').should('include', `/${image}`)
    })

    it('Opened file must appear in home page history section', () => {
        if (cy.firstRunResult) return

        cy.visit('home')

        cy.get('[data-cy="history"]').find('[data-cy="title-view"] [data-cy="entry-item"]').each(item => {
            cy.checkItemStructure(item)
        })
    })
})
