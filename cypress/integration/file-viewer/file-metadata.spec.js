describe('File metadata', () => {
    const video = Cypress.env('file-viewer').pathVideo

    const metadataTitle = 'This is an example title'

    before(() => {
        if (cy.firstRunResult) return

        cy.apiLogin()

        cy.request('DELETE', 'api/file-metadata', {
            path: `/media/${video}`
        })
    })

    beforeEach(() => {
        if (cy.firstRunResult) return

        cy.setCookie('auth-token', cy.token)
    })

    it('No metadata is shown', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy^=metadata-area]').should('not.exist')
    })

    it('Metadata panel can be opened', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area]')
    })

    it('Metadata button in header contains correct text', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').should('contain', 'Add metadata')
    })

    it('Buttons exist', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=upload-poster]')
        cy.get('[data-cy^=metadata-area] [data-cy=get-from-wikipedia-button]')
        cy.get('[data-cy^=metadata-area] [data-cy=cancel-button]')
        cy.get('[data-cy^=metadata-area] [data-cy=save-button]')

        cy.get('[data-cy^=metadata-area] [data-cy=show-more-less-button]').click()
        cy.get('[data-cy^=metadata-area] [data-cy=add-link-button]')
    })

    it('Fields exist', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=title-input]')
        cy.get('[data-cy^=metadata-area] [data-cy=release-date-input]')
        cy.get('[data-cy^=metadata-area] [data-cy=language-input]')
        cy.get('[data-cy^=metadata-area] [data-cy=description-input]')

        cy.get('[data-cy^=metadata-area] [data-cy=show-more-less-button]').click()
        cy.get('[data-cy^=metadata-area] [data-cy=data-companies-input]')
        cy.get('[data-cy^=metadata-area] [data-cy=data-genres-input]')
        cy.get('[data-cy^=metadata-area] [data-cy=data-directors-input]')
        cy.get('[data-cy^=metadata-area] [data-cy=data-actors-input]')
    })

    it('Can close metadata panel (via cancel button)', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=cancel-button]').click()

        cy.get('[data-cy^=metadata-area]').should('not.exist')
    })

    it('Can close metadata panel (via button in header)', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area]').should('not.exist')
    })

    it('Can\'t save when title field is empty', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=save-button]').click()

        cy.get('[data-cy^=metadata-area] [data-cy="alert save-error"]').should('contain', 'Title cannot be empty')
    })

    it('Save with just title', () => {
        if (cy.firstRunResult) return

        cy.request('DELETE', 'api/file-metadata', {
            path: `/media/${video}`
        })

        cy.intercept('PATCH', '/api/file-metadata').as('saveMetadata')

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy=title-input]').type(metadataTitle)

        cy.get('[data-cy^=metadata-area] [data-cy=save-button]').click()
        cy.wait('@saveMetadata')

        cy.get('[data-cy^=metadata-area] [data-cy=title]').should('contain', metadataTitle)
    })

    it('Save with every field filled and useful links', () => {
        if (cy.firstRunResult) return

        cy.request('DELETE', 'api/file-metadata', {
            path: `/media/${video}`
        })

        cy.intercept('PATCH', '/api/file-metadata').as('saveMetadata')

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=title-input]').type(metadataTitle)
        cy.get('[data-cy^=metadata-area] [data-cy=release-date-input]').type("2021-05-28")
        cy.get('[data-cy^=metadata-area] [data-cy=language-input]').type("English")
        cy.get('[data-cy^=metadata-area] [data-cy=description-input]').type("A nice sample video")

        cy.get('[data-cy^=metadata-area] [data-cy=show-more-less-button]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=data-companies-input]').type("Contoso")
        cy.get('[data-cy^=metadata-area] [data-cy=data-genres-input]').type("Sample,Demo")
        cy.get('[data-cy^=metadata-area] [data-cy=data-directors-input]').type("Me,Myself")
        cy.get('[data-cy^=metadata-area] [data-cy=data-actors-input]').type("Everyone,Everybody")

        cy.get('[data-cy^=metadata-area] [data-cy=add-link-button]').click()
        cy.get('[data-cy^=metadata-area] [data-cy=useful-links-editable] :nth-child(2) ').type("MSL Website")
        cy.get('[data-cy^=metadata-area] [data-cy=useful-links-editable] :nth-child(3) ').type("https://msl.francescosorge.com")

        cy.get('[data-cy^=metadata-area] [data-cy=add-link-button]').click()
        cy.get('[data-cy^=metadata-area] [data-cy=useful-links-editable] :nth-child(5) ').type("MSL GitLab")
        cy.get('[data-cy^=metadata-area] [data-cy=useful-links-editable] :nth-child(6) ').type("https://gitlab.com/media-server-lite/media-server-lite")

        cy.get('[data-cy^=metadata-area] [data-cy=save-button]').click()
        cy.wait('@saveMetadata')

        validateMetadata()
    })

    it('Metadata are correctly saved to DB and retrieved on page load', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)
        cy.get('[data-cy^=metadata-area] [data-cy=show-more-less-button]').click()

        validateMetadata()

        cy.request('DELETE', 'api/file-metadata', {
            path: `/media/${video}`
        })
    })

    it('Poster upload', () => {
        if (cy.firstRunResult) return

        posterUploadFull()
    })

    it('Poster remove', () => {
        if (cy.firstRunResult) return

        posterUploadFull()

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=remove-poster]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=remove-poster]').should('not.exist')

        cy.get('[data-cy^=metadata-area] [data-cy=save-button]').click()
        cy.wait('@saveMetadata')

        cy.get('[data-cy=poster]').should('not.exist')
    })

    it('[Get from Wikipedia] Missing title', () => {
        if (cy.firstRunResult) return

        cy.request('DELETE', 'api/file-metadata', {
            path: `/media/${video}`
        })

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=get-from-wikipedia-button]').click()

        cy.get('[data-cy^=metadata-area] [data-cy="alert wikipedia-error"]').should('contain', 'Title cannot be empty')
    })

    it('[Get from Wikipedia] Captain Marvel movie (name match)', () => {
        if (cy.firstRunResult) return

        cy.request('DELETE', 'api/file-metadata', {
            path: `/media/${video}`
        })

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=title-input]').type('Captain Marvel (film)') // Title is required

        validatePostWikipedia()

        cy.request('DELETE', 'api/file-metadata', {
            path: `/media/${video}`
        })
    })

    it('[Get from Wikipedia] Iron Man movie (URL match)', () => {
        if (cy.firstRunResult) return

        cy.request('DELETE', 'api/file-metadata', {
            path: `/media/${video}`
        })

        cy.visit(`f/${video}`)

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=title-input]').type('https://en.wikipedia.org/wiki/Iron_Man_(2008_film)') // Title is required

        validatePostWikipedia()

        cy.request('DELETE', 'api/file-metadata', {
            path: `/media/${video}`
        })
    })

    const validateMetadata = () => {
        cy.get('[data-cy^=metadata-area] [data-cy=title]').should('contain', metadataTitle)
        cy.get('[data-cy^=metadata-area] [data-cy=release-date]').should('contain', '2021-05-28')
        cy.get('[data-cy^=metadata-area] [data-cy=language]').should('contain', 'English')
        cy.get('[data-cy^=metadata-area] [data-cy=description]').should('contain', 'A nice sample video')

        cy.get('[data-cy^=metadata-area] [data-cy=data-companies]').should('contain', 'Contoso')
        cy.get('[data-cy^=metadata-area] [data-cy=data-genres]').should('contain', 'Sample, Demo')
        cy.get('[data-cy^=metadata-area] [data-cy=data-directors]').should('contain', 'Me, Myself')
        cy.get('[data-cy^=metadata-area] [data-cy=data-actors]').should('contain', 'Everyone, Everybody')
        cy.get('[data-cy^=metadata-area] [data-cy=useful-links]').should('contain', 'MSL Website, MSL GitLab')
    }

    const posterUploadFull = () => {
        cy.request('DELETE', 'api/file-metadata', {
            path: `/media/${video}`
        })

        cy.intercept('PATCH', '/api/file-metadata').as('saveMetadata')
        cy.intercept('POST', '/api/file-metadata/poster').as('saveMetadataPoster')

        cy.visit(`f/${video}`)

        cy.get('[data-cy=poster]').should('not.exist')

        cy.get('[data-cy="header"] [data-cy="button metadata"]').click()

        cy.get('[data-cy^=metadata-area] [data-cy=title-input]').type(metadataTitle) // Title is required

        cy.get('[data-cy^=metadata-area] [data-cy=upload-poster]')
        cy.get('[data-cy^=metadata-area] [data-cy=poster-uploader]').attachFile('image.png')

        cy.get('[data-cy^=metadata-area] [data-cy=upload-poster]')
        cy.get('[data-cy^=metadata-area] [data-cy=remove-poster]')

        cy.get('[data-cy^=metadata-area] [data-cy=save-button]').click()
        cy.wait('@saveMetadata')
        cy.wait('@saveMetadataPoster')

        cy.get('[data-cy^=metadata-area] [data-cy=title]').should('contain', metadataTitle)
        cy.get('[data-cy=poster]')
    }

    const validatePostWikipedia = () => {
        cy.intercept('POST', '/api/file-metadata/wikipedia').as('getFromWikipedia')

        cy.get('[data-cy^=metadata-area] [data-cy=get-from-wikipedia-button]').click()
        cy.wait('@getFromWikipedia')

        cy.get('[data-cy^=metadata-area] [data-cy=poster]')
        cy.get('[data-cy^=metadata-area] [data-cy=title]')
        cy.get('[data-cy^=metadata-area] [data-cy=description]')

        cy.get('[data-cy^=metadata-area] [data-cy=show-more-less-button]').click()
        cy.get('[data-cy^=metadata-area] [data-cy=data-companies]')
        cy.get('[data-cy^=metadata-area] [data-cy=data-directors]')
        cy.get('[data-cy^=metadata-area] [data-cy=data-actors]')
        cy.get('[data-cy^=metadata-area] [data-cy=useful-links]')
    }
})
