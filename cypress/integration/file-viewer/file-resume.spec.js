describe('File resume', () => {
    const video = Cypress.env('file-viewer').pathVideo
    const minSecondsToResumePatch = (Cypress.env('resumes')?.minSecondsToPatch ?? 10) + 1

    before(() => {
        if (cy.firstRunResult) return

        cy.apiLogin()
    })

    beforeEach(() => {
        if (cy.firstRunResult) return

        cy.setCookie('auth-token', cy.token)
    })

    it('No resume dialog is shown', () => {
        if (cy.firstRunResult) return

        cy.request('DELETE', 'api/resumes', {
            path: `/media/${video}`
        })

        noResumeDialogCheck()
    })

    it('Wait for resume trigger', () => {
        if (cy.firstRunResult) return

        cy.request('DELETE', 'api/resumes', {
            path: `/media/${video}`
        })

        cy.visit(`f/${video}`)

        cy.get('[data-cy=video-player]').then((videoPlayer) => {
            cy.wait(1000)

            cy.intercept('PATCH', '/api/resumes').as('saveResume')

            videoPlayer = videoPlayer.get(0)
            videoPlayer.pause()
            videoPlayer.currentTime = minSecondsToResumePatch

            cy.wait('@saveResume', { timeout: minSecondsToResumePatch * 2 * 1000 })
        })
    })

    it('Resume dialog is shown', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="dialog resume"]')
        cy.get('[data-cy="dialog resume"] [data-cy="button start-from-beginning"]')
        cy.get('[data-cy="dialog resume"] [data-cy="button resume-from-last-time"]')
    })

    it('Can close dialog', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="dialog resume"] [data-cy="button close"]').click()
        cy.get('[data-cy="dialog resume"]').should('not.exist')
    })

    it('Can start from beginning', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="dialog resume"] [data-cy="button start-from-beginning"]').click()
        cy.get('[data-cy="dialog resume"]').should('not.exist')

        cy.get('[data-cy=video-player]').then((videoPlayer) => {
            videoPlayer = videoPlayer.get(0)
            videoPlayer.pause()

            expect(videoPlayer.currentTime).to.equal(0)
        })
    })

    it('Can resume', () => {
        if (cy.firstRunResult) return

        cy.visit(`f/${video}`)

        cy.get('[data-cy="dialog resume"] [data-cy="button resume-from-last-time"]').click()
        cy.get('[data-cy="dialog resume"]').should('not.exist')

        cy.get('[data-cy=video-player]').then((videoPlayer) => {
            videoPlayer = videoPlayer.get(0)
            videoPlayer.pause()

            expect(videoPlayer.currentTime).to.equal(minSecondsToResumePatch)
        })
    })

    it('Can delete resume by reaching the end of the video', () => {
        if (cy.firstRunResult) return

        cy.request('DELETE', 'api/resumes', {
            path: `/media/${video}`
        })

        cy.visit(`f/${video}`)

        cy.get('[data-cy=video-player]').then((videoPlayer) => {
            cy.wait(1000)

            cy.intercept('DELETE', '/api/resumes').as('deleteResume')

            videoPlayer = videoPlayer.get(0)
            videoPlayer.pause()
            videoPlayer.currentTime = videoPlayer.duration

            cy.wait('@deleteResume', { timeout: minSecondsToResumePatch * 2 * 1000 })
        })

        noResumeDialogCheck()
    })

    const noResumeDialogCheck = () => {
        cy.visit(`f/${video}`)

        cy.get('[data-cy="dialog resume"]').should('not.exist')
    }
})
