<?php

namespace App\Scrapers;

class WikipediaEN extends AWikipedia
{
    protected string $titlePath = '.infobox-above.summary';
    protected string $posterPath = '.infobox-image img';
    protected string $infoBoxPath = '.infobox > tbody > tr';
    protected string $plotPath = '#Plot';
    protected array $scrapMatches = [
        'directed by' => 'directors',
        'starring' => 'actors',
        'productioncompany' => 'companies',
        'productioncompanies' => 'companies'
    ];

    public function matches(string $url): bool
    {
        return str_starts_with($url, 'https://en.wikipedia.org/wiki');
    }
}
