<?php

namespace App\Scrapers;

class WikipediaIT extends AWikipedia
{

    protected string $titlePath = '.infobox.sinottico .sinottico_testata';
    protected string $posterPath = '.sinottico_testo_centrale img';
    protected string $infoBoxPath = '.infobox > tbody > tr';
    protected string $plotPath = '#Trama';
    protected array $scrapMatches = [
        'regia' => 'directors',
        'casa di produzione' => 'companies'
    ];

    public function matches(string $url): bool
    {
        return str_starts_with($url, 'https://it.wikipedia.org/wiki');
    }
}
