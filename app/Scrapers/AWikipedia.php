<?php

namespace App\Scrapers;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

abstract class AWikipedia implements IScraper
{

    protected string $titlePath;
    protected string $posterPath;
    protected string $infoBoxPath;
    protected string $plotPath;
    protected array $scrapMatches;

    public function scrap(string $url): ?array
    {
        $client = new Client();
        $crawler = $client->request('GET', $url);

        $payload = [];

        $title = $crawler->filter($this->titlePath);
        if ($title->count() >= 1) {
            $payload['title'] = $title->first()->text();
        }

        $image = $crawler->filter($this->posterPath);
        if ($image->count() >= 1) {
            $payload['poster'] = "https:" . $image->first()->attr('src');
        }

        $this->infoBox($crawler, $payload);

        $this->plot($crawler, $payload);

        if (!empty($payload) && isset($payload['title'])) {
            if (!isset($payload['links'])) {
                $payload['links'] = [];
            }

            array_push($payload['links'], [
                "link" => $url,
                "text" => "Wikipedia"
            ]);

            return $payload;
        }

        return null;
    }

    private function infoBox($crawler, &$payload)
    {
        $infobox = $crawler->filter($this->infoBoxPath);
        $infobox->each(function (Crawler $node) use (&$payload) {
            $label = $node->filter('th');

            if ($label->count() > 0) {
                $lblText = strtolower(trim($label->text()));

                if (array_key_exists($lblText, $this->scrapMatches)) {
                    $value = $node->filter('td');

                    if ($value->count() > 0) {
                        $values = [];

                        $value->filterXPath('descendant-or-self::a[not(.//ul)] | descendant-or-self::ul/li')
                            ->each(function (Crawler $n) use (&$values) {
                                if (!in_array($n->text(), $values)) {
                                    array_push($values, $n->text());
                                }
                            });

                        $payload[$this->scrapMatches[$lblText]] = $values;
                    }
                }
            }
        });
    }

    private function plot($crawler, &$payload)
    {
        $plot = $crawler->filter($this->plotPath);
        if ($plot->count() > 0) {
            $plot = $plot->ancestors()->eq(0)->nextAll()->filter('p');

            $plotParagraphs = [];

            if ($plot->count() >= 1) {
                array_push($plotParagraphs, $plot->eq(0)->text());
            }

            if ($plot->count() >= 2) {
                array_push($plotParagraphs, $plot->eq(1)->text());
            }

            $payload['description'] = implode("\r\n", $plotParagraphs);
        }
    }

}
