<?php

namespace App\Scrapers;

interface IScraper
{

    public function matches(string $url): bool;

    public function scrap(string $url): ?array;

}
