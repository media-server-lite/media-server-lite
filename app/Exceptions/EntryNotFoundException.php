<?php
namespace App\Exceptions;

use App\Http\Helpers\Filesystem\Helper as FileSystemHelper;
use App\Models\LostAndFound;
use Exception;
use Illuminate\Support\Facades\Log;

class EntryNotFoundException extends Exception
{

    public function __construct(string $path)
    {
        $path = FileSystemHelper::normalizePath($path);

        Log::error("Entry \"$path\" not found. Sending to lost&found...");

        $laf = LostAndFound::firstOrNew(
            ['path' => $path],
            ['path' => $path]
        );

        $laf->save();
    }

}

