<?php

namespace App\Console\Commands\User;

use App\Models\Group;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Create extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var
     */
    protected $description = 'Creates a new standard Media Server Lite user';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->line("Will be showed in the login page and in the navbar");
        $name = (string) $this->ask("Name");

        do {
            $randomUuid = Str::uuid();
            $this->line("Username is used by the application itself and will never be visibile in the interface. You are free to choose a custom username (which must be unique) or let us generate a random one for you (we generated $randomUuid)");
            $username = (string) $this->ask("Username", (string) $randomUuid);

            $unique = User::where('username', $username)->first() === null;

            if (!$unique) {
                $this->error("Username already exists. Please try another one.");
            }
        } while (!$unique);

        $this->line("The PIN (numeric only password) required to access your account. You are free to leave it empty, this way your account will not be password protected and anyone will be able to access");
        $password = (string) $this->secret("Password");

        if ($password) {
            $password = Hash::make($password);
        }

        $this->info("\nUser summary:");
        $this->line("Name: $name");
        $this->line("Username: $username");
        $this->line("Password: " . ($password ? "The one you chose" : "No password"));

        if ($this->confirm('Shall we create the account?')) {
            /** @var Group */
            $group = Group::where('name', 'users')->first(['id']);

            $user = new User();
            $user->name = $name;
            $user->username = $username;
            $user->password = $password;
            $user->group_id = $group->id;
            $user->save();

            $this->info("User created successfully!");

            return 0;
        } else {
            $this->error("Aborted. Account not created.");

            return 1;
        }
    }
}
