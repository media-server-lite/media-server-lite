<?php

namespace App\Console\Commands;

use App\Models\GroupPolicy;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class DistUpgrade extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'distupgrade';

    /**
     * The console command description.
     *
     * @var
     */
    protected $description = 'Updates group policies to latest version available';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** @var string */
        $appName = config('msl.app_name', 'Media Server Lite');

        $this->line('Starting ' . $appName . ' dist-upgrade');


        // Do database migrations
        $this->info("Running database migrations...");

        Artisan::call('migrate', ['--force' => true]);

        $this->info("Migrations done!" . PHP_EOL);


        // Insert new group policies
        $this->info("Upgrading group policies...");

        $groupPolicies = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "database" . DIRECTORY_SEPARATOR . "seeders" . DIRECTORY_SEPARATOR . "group-policies.json");

        if ($groupPolicies === false) {
            $this->error('Something bad happened and couldn\'t install default Group Policies');
            die();
        }

        $groupPolicies = (array) json_decode($groupPolicies, true);

        $this->withProgressBar($groupPolicies, function (array $gp) {
            if (!GroupPolicy::where('path', $gp['path'])->first()) {
                GroupPolicy::insert($gp);
            }
        });
        $this->info(" Done!");


        // Delete dangling Group Policies
        $this->info(PHP_EOL . "Deleting old group policies...");

        GroupPolicy::whereNotIn(
            'path',
            array_map(
                /**
                 * @return mixed
                 */
                fn (array $gp) => $gp['path'],
                $groupPolicies
            )
        )->delete();

        $this->info("Done!");


        // Done!
        $this->info(PHP_EOL . "Dist Upgrade completed successfully!" . PHP_EOL);

        return 0;
    }
}
