<?php

namespace App\Providers;

use App\Models\GroupPolicy;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Inertia\Inertia;
use Tymon\JWTAuth\Facades\JWTAuth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            if (Schema::hasTable('group_policies')) {
                /** @psalm-suppress TooManyTemplateParams */
                config([
                    'msl' => \App\Models\GroupPolicy::all([
                        'path', 'value', 'type'
                    ])
                        ->keyBy('path') // key every gp by its name
                        ->transform(function (GroupPolicy $gp) {
                            if ($gp->type === 'bool') {
                                return $gp->value === '1' ? true : false;
                            } else if ($gp->type === 'int') {
                                return (int)$gp->value;
                            } else if ($gp->type === 'float') {
                                return (float)$gp->value;
                            } else if ($gp->type === 'array') {
                                return json_decode((string) $gp->value, true);
                            }

                            return $gp->value; // return only the value
                        })
                        ->toArray() // make it an array
                ]);

                Inertia::share('mslAppName', config('msl.app_name'));

                define('MSL_APP_MEDIA_DIRECTORY', config('msl.app_media-directory'));
            }
        } catch (\Exception $e) {
            // Crashes when Sqlite DB path is not correct for the console. We do not need to log that since it's not exactly a bug
        }
    }
}
