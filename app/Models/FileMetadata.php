<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileMetadata extends Model
{
    use HasFactory;

    protected $table = 'files_metadata';

    protected $casts = [
        'directors' => 'array',
        'actors' => 'array',
        'companies' => 'array',
        'genres' => 'array',
        'links' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var
     */
    protected $fillable = [
        'path',
        'title',
        'release_date',
        'poster',
        'directors',
        'actors',
        'companies',
        'genres',
        'links',
        'description',
        'language'
    ];
}
