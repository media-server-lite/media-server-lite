<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LostAndFound extends Model
{
    use HasFactory;

    protected $table = 'losts_and_founds';

    /**
     * The attributes that are mass assignable.
     *
     * @var
     */
    protected $fillable = [
        'path',
    ];
}
