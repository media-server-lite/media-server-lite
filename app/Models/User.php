<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var
     */
    protected $fillable = [
        'name',
        'username',
        'password',
        'group_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = ['has_password', 'profile_picture'];

    protected array $linked_tables = ['collections', 'favorites', 'histories'];

    public function getHasPasswordAttribute(): bool
    {
        return $this->password !== null;
    }

    public function getProfilePicture(string $color = 'black'): string {
        $path = "/images/avatars/$this->id.png";
        if (file_exists(public_path() . $path)) {
            return $path;
        }

        return "/images/avatar_$color.png";
    }

    public function getProfilePictureAttribute(): string {
        return $this->getProfilePicture();
    }

    public function getLinkedTables(): array {
        return $this->linked_tables;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     * @psalm-suppress UndefinedThisPropertyFetch
     */
    public function getJWTCustomClaims()
    {
        return [
            'user_id' => $this->id
        ];
    }
}
