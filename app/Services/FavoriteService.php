<?php

namespace App\Services;

use App\Exceptions\EntryNotFoundException;
use App\Models\Favorite;
use App\Http\Helpers\Filesystem\Helper as FileSystemHelper;
use App\Http\Helpers\Filesystem\Folder;
use App\Http\Helpers\Filesystem\File;

class FavoriteService
{
    public function getByUserId(int $user_id): array
    {
        /** @var array<int, Favorite> */
        $favorites = Favorite::where('user_id', $user_id)->get(['path'])->toArray();

        foreach ($favorites as $i => &$entry) {
            $path = MSL_APP_MEDIA_DIRECTORY . DIRECTORY_SEPARATOR . (string) $entry['path'];

            try {
                $e = is_dir($path) ? new Folder($path) : new File($path);
                $entry = FileSystemHelper::entryToPublic($e);
            } catch (EntryNotFoundException $_) {
                array_splice($favorites, $i, 1);
            }
        }

        return $favorites;
    }

    public function favorite(string $path, bool $fav, int $userId): string
    {
        $favorite = Favorite::where('user_id', $userId)->where('path', $path)->first();

        if ($favorite) {
            if (!$fav) {
                $favorite->delete();
                return 'removed';
            }
        } else {
            if ($fav) {
                $favorite = new Favorite();
                $favorite->path = $path;
                $favorite->user_id = $userId;
                $favorite->save();
                return 'created';
            }
        }

        return 'untouched';
    }
}
