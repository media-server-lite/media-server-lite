<?php

namespace App\Services;

use App\Exceptions\EntryNotFoundException;
use App\Http\Helpers\Filesystem\Helper as FileSystemHelper;
use App\Http\Helpers\Filesystem\File;
use App\Http\Helpers\Filesystem\Folder;
use App\Models\History;

class HistoryService
{
    public function getByUserIdLimit(int $userId, int $limit = 10, string $orderBy = 'updated_at'): array
    {
        if ($orderBy !== 'counter' && $orderBy !== 'updated_at') {
            $orderBy = 'updated_at';
        }

        $history = History::where('user_id', $userId)->orderBy($orderBy, 'desc');

        if ($limit > 0) {
            $history = $history->limit($limit);
        }

        /** @var array<int, History> */
        $history = $history->get(['path'])->toArray();

        foreach ($history as $i => &$entry) {
            $path = MSL_APP_MEDIA_DIRECTORY . DIRECTORY_SEPARATOR . (string) $entry['path'];

            try {
                $e = is_dir($path) ? new Folder($path) : new File($path);
                $entry = FileSystemHelper::entryToPublic($e);
            } catch (EntryNotFoundException $_) {
                array_splice($history, $i, 1);
            }
        }

        return $history;
    }

    public function increaseCounter(string $path, int $userId): bool
    {
        /** @var History */
        $history = History::firstOrNew(
            ['path' => $path, 'user_id' => $userId],
            ['counter' => 0]
        );

        $history->counter = (int) $history->counter + 1;
        $history->save();

        return true;
    }

    public function clearAll(int $userId): bool {
        History::where('user_id', $userId)->delete();

        return true;
    }
}
