<?php

namespace App\Services;

use App\Models\Resume;

class ResumeService
{

    /**
     * @return int | null
     */
    public function get(int $userId, string $path)
    {
        $resume = Resume::where('user_id', $userId)->where('path', $path)->first(['seconds']);

        if ($resume === null) {
            return null;
        }

        return $resume['seconds'];
    }

    public function patch(int $userId, string $path, float $seconds): bool
    {
        /** @var Resume */
        $resume = Resume::firstOrNew(
            ['path' => $path, 'user_id' => $userId]
        );

        $resume->seconds = $seconds;
        $resume->save();

        return true;
    }

    public function delete(int $userId, string $path): bool
    {
        Resume::where('user_id', $userId)->where('path', $path)->delete();

        return true;
    }
}
