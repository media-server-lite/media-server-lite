<?php

namespace App\Services;

use App\Models\Collection;
use App\Models\CollectionPairs;

class CollectionsService
{

    private function existsByName(string $name, int $userId): bool
    {
        return Collection::where('user_id', $userId)->where('name', $name)->exists();
    }

    private function existsById(int $id, int $userId): bool
    {
        return Collection::where('user_id', $userId)->where('id', $id)->exists();
    }

    public function getByUserId(int $user_id): array
    {
        return Collection::where('user_id', $user_id)->orderBy('name')->get(['id', 'name', 'icon'])->toArray();
    }

    public function checkIfCollectionsHavePath(array $collections, string $path): array
    {
        $result = [];

        /** @var Collection */
        foreach ($collections as $collection) {
            array_push($result, [
                'collection_id' => $collection,
                'exists' => CollectionPairs::where('collection_id', $collection)->where('path', $path)->exists()
            ]);
        }

        return $result;
    }

    public function pair(bool $add, int $collectionId, string $path, int $userId): string
    {
        if (!$this->existsById($collectionId, $userId)) {
            return 'collection_not_found';
        }

        $pair = CollectionPairs::where('path', $path)->where('collection_id', $collectionId)->first();

        if ($pair) {
            if (!$add) {
                $pair->delete();
                return 'removed';
            }
        } else {
            if ($add) {
                $pair = new CollectionPairs();
                $pair->path = $path;
                $pair->collection_id = $collectionId;
                $pair->save();
                return 'added';
            }
        }

        return 'untouched';
    }

    public function new(?string $name, ?string $icon, int $userId): array
    {
        if ($name === null || strlen(trim($name)) === 0) {
            return ['status' => 'error', 'error' => 'empty-name'];
        }

        if ($this->existsByName($name, $userId)) {
            return ['status' => 'error', 'error' => 'already-exists'];
        }

        $collection = Collection::create(['name' => $name, 'icon' => $icon, 'user_id' => $userId]);
        $collection = $collection->fresh();

        return ['status' => 'created', 'collection' => $collection];
    }

    public function getById(int $id, int $userId): array {
        if (!$this->existsById($id, $userId)) {
            return ['status' => 'error', 'error' => 'collection_not_found'];
        }

        return Collection::find($id)->toArray();
    }

    public function getPairsByCollectionId(int $id, int $userId): array {
        if (!$this->existsById($id, $userId)) {
            return ['status' => 'error', 'error' => 'collection_not_found'];
        }

        return CollectionPairs::where('collection_id', $id)->get()->toArray();
    }

    public function renameCollection(int $id, ?string $name, int $userId): array {
        if (!$this->existsById($id, $userId)) {
            return ['status' => 'error', 'error' => 'collection_not_found'];
        }

        if ($name === null || strlen(trim($name)) === 0) {
            return ['status' => 'error', 'error' => 'empty-name'];
        }

        if ($this->existsByName($name, $userId)) {
            return ['status' => 'error', 'error' => 'already-exists'];
        }

        Collection::find($id)->update(['name' => $name]);

        return ['status' => 'renamed'];
    }

    public function deleteById(int $id, int $userId): array {
        if (!$this->existsById($id, $userId)) {
            return ['status' => 'error', 'error' => 'collection_not_found'];
        }

        CollectionPairs::where('collection_id', $id)->delete();
        Collection::find($id)->delete();

        return ['status' => 'deleted'];
    }
}
