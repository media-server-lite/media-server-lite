<?php

namespace App\Services\Admin;

use App\Models\Group;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AdminService
{

    protected string $ADMIN_GROUP_NAME = 'admins';

    private function getGroup(): Group
    {
        return Group::where('name', $this->ADMIN_GROUP_NAME)->first();
    }

    public function getByUsername(string $username, bool $ignoreGroup = false): ?User
    {
        $query = User::where('username', $username);

        if (!$ignoreGroup) {
            $query = $query->where('group_id', $this->getGroup()->id);
        }

        return $query->first();
    }

    public function create(string $name, string $username, string $password): array
    {
        if ($this->getByUsername($username, true) !== null) {
            return ['status' => 'error', 'error' => 'username-already-exists'];
        }

        if (!ctype_alnum($username)) {
            return ['status' => 'error', 'error' => 'username-not-valid'];
        }

        $user = User::create(
            [
                'name' => $name,
                'username' => $username,
                'password' => Hash::make($password),
                'group_id' => $this->getGroup()->id
            ]
        );
        $user = $user->fresh();

        return ['status' => 'created', 'user' => $user];
    }

    public function count(): int
    {
        return User::where('group_id', $this->getGroup()->id)->count();
    }
}
