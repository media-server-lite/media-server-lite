<?php

namespace App\Services\Admin;

use App\Models\GroupPolicy;
use Carbon\CarbonImmutable;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class AppService
{

    protected array $REGEX_GPS = ['app_banned-entries'];

    public function updateGroupPolicy(array $gps): array
    {
        $warnings = [];

        /** @var string */
        foreach ($gps as $path => $value) {
            $gp = GroupPolicy::where('path', $path)->first();

            if ($gp === null) {
                array_push($warnings, "No group policy with path $path found");
            } else {
                if (in_array($path, $this->REGEX_GPS) && $value === '//') {
                    $value = null;
                }

                $gp->value = $value;
                $gp->save();
            }
        }

        return ['status' => 'updated', 'warnings' => $warnings];
    }

    public function oobe(string $appName, string $bannedEntries, string $loginAutoSelect): array
    {
        Artisan::call('distupgrade');

        $gps = [
            'app_name' => $appName,
            'app_banned-entries' => $bannedEntries,
            'app_login_auto-select' => $loginAutoSelect === 'true' ? '1' : '0'
        ];

        $update = $this->updateGroupPolicy($gps);

        Storage::put('did-first-run.json', json_encode(
            [
                "timestamp" => CarbonImmutable::now()
            ]
        ));

        return $update;
    }

    public function isFirstRun(): bool {
        return !Storage::exists('did-first-run.json');
    }
}
