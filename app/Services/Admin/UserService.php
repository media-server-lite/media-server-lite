<?php

namespace App\Services\Admin;

use App\Models\Group;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserService
{

    protected string $USERS_GROUP_NAME = 'users';
    protected string $AVATAR_DIR = 'images/avatars';

    private function getGroup(): Group
    {
        /** @var Group */
        return Group::where('name', $this->USERS_GROUP_NAME)->first();
    }

    public function getById(int $id, bool $ignoreGroup = false): ?User
    {
        $query = User::where('id', $id);

        if (!$ignoreGroup) {
            $query = $query->where('group_id', $this->getGroup()->id);
        }

        return $query->first();
    }

    public function getByUsername(string $username, bool $ignoreGroup = false): ?User
    {
        $query = User::where('username', $username);

        if (!$ignoreGroup) {
            $query = $query->where('group_id', $this->getGroup()->id);
        }

        return $query->first();
    }

    public function create(string $name, string $username, ?string $password): array
    {
        if ($this->getByUsername($username, true) !== null) {
            return ['status' => 'error', 'error' => 'username-already-exists'];
        }

        if (!ctype_alnum($username)) {
            return ['status' => 'error', 'error' => 'username-not-valid'];
        }

        $user = User::create(
            [
                'name' => $name,
                'username' => $username,
                'password' => $password === null ? null : Hash::make($password),
                'group_id' => $this->getGroup()->id
            ]
        );
        $user = $user->fresh();

        return ['status' => 'created', 'user' => $user];
    }

    public function count(): int
    {
        return User::where('group_id', $this->getGroup()->id)->count();
    }

    public function uploadProfilePicture(int $userId, UploadedFile $file): array
    {
        Storage::disk('public_bypass')->putFileAs("$this->AVATAR_DIR", $file, "$userId.png");

        return ['status' => 'uploaded'];
    }

    public function getAll(): array
    {
        return User::where('group_id', $this->getGroup()->id)->get()->toArray();
    }

    public function deleteProfilePicture(int $userId): bool
    {
        Storage::disk('public_bypass')->delete("$this->AVATAR_DIR/$userId.png");

        return true;
    }

    /**
     * @psalm-suppress UndefinedMagicPropertyFetch
     */
    public function deleteByUsername(string $username, bool $cascade = false): array
    {
        $query = User::where('username', $username);

        if ($cascade) {
            /** @var User */
            $user = $query->first();

            $linkedTables = $user->getLinkedTables();

            /** @var string */
            foreach ($linkedTables as $table) {
                DB::table($table)->where('user_id', $user->id)->delete();
            }
        }

        $this->deleteProfilePicture($query->first()->id);

        $query->delete();

        return ['status' => 'deleted'];
    }

    public function basic(string $name, bool $clearProfilePicture, int $userId): array
    {
        $user = User::find($userId);

        $user->name = $name;

        if ($clearProfilePicture) {
            $this->deleteProfilePicture($userId);
        }

        $user->update();

        return ['status' => 'updated', 'user' => $user];
    }

    public function changePassword(string $currentPassword, string $newPassword, string $newPasswordRepeat, int $userId): array
    {
        $user = User::find($userId);

        if ($user->has_password && !Hash::check($currentPassword, $user->password)) {
            return ['status' => 'error', 'error' => 'password-not-correct'];
        }

        if ($newPassword !== $newPasswordRepeat) {
            return ['status' => 'error', 'error' => 'password-mismatch'];
        }

        $user->password = Hash::make($newPassword);
        $user->update();

        return ['status' => 'updated'];
    }

    public function removePassword(string $currentPassword, int $userId): array
    {
        $user = User::find($userId);

        if (!Hash::check($currentPassword, $user->password)) {
            return ['status' => 'error', 'error' => 'password-not-correct'];
        }

        $user->password = null;
        $user->update();

        return ['status' => 'removed'];
    }
}
