<?php

namespace App\Services;

use App\Models\FileMetadata;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileMetadataService
{

    protected const POSTER_DIR = 'images/posters';

    /**
     * @return FileMetadata | null
     */
    public function get(string $path)
    {
        $fileMetadata = FileMetadata::where('path', $path)->first();

        if ($fileMetadata === null) {
            return null;
        }

        return $fileMetadata;
    }

    public function exists(string $path): bool
    {
        return FileMetadata::where('path', $path)->exists();
    }

    public function patch(string $path, array $payload): FileMetadata
    {
        /** @var FileMetadata */
        $fileMetadata = FileMetadata::firstOrNew(
            ['path' => $path]
        );

        if (isset($payload['clearPoster']) && $payload['clearPoster'] === true && $fileMetadata->poster !== null) {
            $this->deletePoster($fileMetadata['poster']);
            $fileMetadata->poster = null;
        }

        if (array_key_exists('actors', $payload)) {
            $fileMetadata->actors = $payload['actors'];
        }
        if (array_key_exists('companies', $payload)) {
            $fileMetadata->companies = $payload['companies'];
        }
        if (array_key_exists('description', $payload)) {
            $fileMetadata->description = $payload['description'];
        }
        if (array_key_exists('directors', $payload)) {
            $fileMetadata->directors = $payload['directors'];
        }
        if (array_key_exists('genres', $payload)) {
            $fileMetadata->genres = $payload['genres'];
        }
        if (array_key_exists('language', $payload)) {
            $fileMetadata->language = $payload['language'];
        }
        if (array_key_exists('links', $payload)) {
            $fileMetadata->links = $payload['links'];
        }
        if (array_key_exists('release_date', $payload)) {
            $fileMetadata->release_date = $payload['release_date'];
        }
        if (array_key_exists('title', $payload)) {
            $fileMetadata->title = $payload['title'];
        }
        if (array_key_exists('poster', $payload) && isset($payload['poster'])) {
            $fileMetadata->poster = $payload['poster'];
        }

        $fileMetadata->save();

        return $fileMetadata;
    }

    public function delete(string $path): bool
    {
        /** @var FileMetadata */
        $fileMetadata = FileMetadata::where('path', $path);

        if ($fileMetadata->exists()) {
            $fileMetadata->delete();
            return true;
        } else {
            return false;
        }
    }

    public function uploadPoster(string $path, UploadedFile $file): array
    {
        $id = (int) $this->get($path)['id'];

        Storage::disk('public_bypass')->putFileAs(self::POSTER_DIR, $file, "$id." . $file->extension());

        $this->patch($path, ['poster' => self::POSTER_DIR . DIRECTORY_SEPARATOR . "$id." . $file->extension()]);

        return ['status' => 'uploaded'];
    }

    /**
     * @param string $path the path of the poster to remove
     */
    public function deletePoster(string $path): bool
    {
        Storage::disk('public_bypass')->delete("$path");

        return true;
    }

}
