<?php

namespace App\Http\Controllers;

use App\Http\CustomModels\BaseResponseDto;
use App\Http\Helpers\Filesystem\Helper as FilesystemHelper;
use App\Scrapers\IScraper;
use App\Scrapers\WikipediaEN;
use App\Services\FileMetadataService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use HaydenPierce\ClassFinder\ClassFinder;

class FileMetaDataController extends Controller
{

    protected FileMetadataService $fileMetaDataService;

    public function __construct(FileMetadataService $fileMetaDataService)
    {
        $this->fileMetaDataService = $fileMetaDataService;
    }

    public function get(Request $request): JsonResponse
    {
        if (!$request->path) {
            return response()->json(
                new BaseResponseDto(
                    'You must provide a path',
                    400,
                    'bad-request'
                )
            );
        }

        $path = (string) $request->path;
        /** @var boolean */
        $addMedia = $request->addMedia;

        if ($addMedia) {
            $path = DIRECTORY_SEPARATOR . MSL_APP_MEDIA_DIRECTORY . DIRECTORY_SEPARATOR . FilesystemHelper::removeIfStartsWith($path, '/');
        }

        return response()->json(
            new BaseResponseDto(
                [
                    'metadata' => $this->fileMetaDataService->get(
                        $path
                    ),
                    'path' => $path
                ]
            )
        );
    }

    public function patch(Request $request): JsonResponse
    {
        if ($request->links !== null) {
            $request->merge([
                'links' => json_decode($request->links, true)
            ]);
        }

        if (!$request->path || !$request->title) {
            return response()->json(
                new BaseResponseDto(
                    'You must provide a path and a title',
                    400,
                    'bad-request'
                )
            );
        }

        return response()->json(
            new BaseResponseDto(
                $this->fileMetaDataService->patch(
                    (string) $request->path,
                    $request->all()
                )
            )
        );
    }

    public function delete(Request $request): JsonResponse
    {
        if (!$request->path) {
            return response()->json(
                new BaseResponseDto(
                    'You must provide a path',
                    400,
                    'bad-request'
                )
            );
        }

        return response()->json(
            new BaseResponseDto(
                $this->fileMetaDataService->delete(
                    (string) $request->path
                )
            )
        );
    }

    public function uploadPoster(Request $request): JsonResponse
    {
        if (!$request->path) {
            return response()->json(
                new BaseResponseDto(
                    'You must provide a path',
                    400,
                    'bad-request'
                )
            );
        }

        /** @var UploadedFile */
        $file = $request->file('poster');

        return response()->json(
            new BaseResponseDto(
                $this->fileMetaDataService->uploadPoster(
                    (string) $request->path,
                    $file
                )
            )
        );
    }

    public function scrap(Request $request): JsonResponse
    {
        if (!$request->path || !$request->url) {
            return response()->json(
                new BaseResponseDto(
                    [
                        'status' => 'error',
                        'message' => 'You must provide a path and a full URL'
                    ],
                    400,
                    'bad-request'
                )
            );
        }

        $scapers = ClassFinder::getClassesInNamespace('App\Scrapers');
        $scraperToUse = null;

        foreach ($scapers as $scraper) {
            try {
                /** @var IScraper */
                $s = new $scraper();
                if ($s->matches($request->url)) {
                    $scraperToUse = $s;
                }
            } catch (\Throwable $e) {
                // Tried to instantiate an interface
            }
        }

        if (!$scraperToUse) {
            return response()->json(
                new BaseResponseDto([
                    'status' => 'error',
                    'message' => 'URL not supported'
                ])
            );
        }

        $payload = $scraperToUse->scrap($request->url);

        return response()->json(
            $payload ?
                new BaseResponseDto([
                    'status' => 'success',
                    'data' => $this->fileMetaDataService->patch(
                        (string) $request->path,
                        $payload
                    )
                ]) :
                new BaseResponseDto([
                    'status' => 'error',
                    'message' => 'scraped_nothing'
                ])
        );
    }
}
