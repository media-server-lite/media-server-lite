<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\CollectionsService;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Services\FavoriteService;
use App\Services\HistoryService;

class HomeController extends Controller
{

    protected FavoriteService $favoriteService;
    protected HistoryService $historyService;
    protected CollectionsService $collectionsService;

    public function __construct(FavoriteService $favoriteService, HistoryService $historyService, CollectionsService $collectionsService)
    {
        $this->favoriteService = $favoriteService;
        $this->historyService = $historyService;
        $this->collectionsService = $collectionsService;
    }

    public function view(Request $request): \Inertia\Response
    {
        /** @var User */
        $jwt = JWTAuth::parseToken()->getPayload();

        return Inertia::render(
            'Home',
            [
                'home' => FolderViewer::makeFolderData($request),
                'favorites' => $this->favoriteService->getByUserId((int) $jwt->get('user_id')),
                'histories' => $this->historyService->getByUserIdLimit((int) $jwt->get('user_id')),
                'collections' => $this->collectionsService->getByUserId((int) $jwt->get('user_id'))
            ]
        );
    }
}
