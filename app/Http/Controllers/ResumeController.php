<?php

namespace App\Http\Controllers;

use App\Http\CustomModels\BaseResponseDto;
use App\Services\ResumeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ResumeController extends Controller
{

    protected ResumeService $resumeService;

    public function __construct(ResumeService $resumeService)
    {
        $this->resumeService = $resumeService;
    }

    public function patch(Request $request): JsonResponse
    {
        if (!$request->path || !$request->seconds) {
            return response()->json(
                new BaseResponseDto(
                    'You must provide a path and seconds', 400, 'bad-request'
                )
            );
        }

        /** @var User */
        $jwt = JWTAuth::parseToken()->getPayload();

        return response()->json(
            new BaseResponseDto(
                $this->resumeService->patch(
                    (int) $jwt->get('user_id'),
                    (string) $request->path,
                    (float) $request->seconds
                )
            )
        );
    }

    public function delete(Request $request): JsonResponse
    {
        if (!$request->path) {
            return response()->json(
                new BaseResponseDto(
                    'You must provide a path', 400, 'bad-request'
                )
            );
        }

        /** @var User */
        $jwt = JWTAuth::parseToken()->getPayload();

        return response()->json(
            new BaseResponseDto(
                $this->resumeService->delete(
                    (int) $jwt->get('user_id'),
                    (string) $request->path
                )
            )
        );
    }
}
