<?php

namespace App\Http\Controllers;

use App\Services\Admin\UserService;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Tymon\JWTAuth\Facades\JWTAuth;

class SettingsController extends Controller
{

    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function view(Request $request): \Inertia\Response
    {
        /** @var User */
        $jwt = JWTAuth::parseToken()->getPayload();

        return Inertia::render(
            'Settings',
            [
                'security-and-password' => [
                    'has_password' => $this->userService->getById(
                        (int) $jwt->get('user_id')
                    )->has_password
                ]
            ]
        );
    }
}
