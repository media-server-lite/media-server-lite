<?php

namespace App\Http\Controllers;

use App\Http\CustomModels\BaseResponseDto;
use App\Models\User;
use App\Services\Admin\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Inertia\Inertia;
use Illuminate\Http\UploadedFile;

class UserController extends Controller
{

    protected int $jwtHoursTTL = 3;

    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @return JsonResponse
     * @psalm-suppress UndefinedPropertyAssignment
     * @psalm-suppress UndefinedMagicPropertyFetch
     */
    public function getLoggable(): JsonResponse
    {
        /** @var array<User> */
        $loggableUsers = DB::table('groups')->where('can_login', true)->join('users', 'users.group_id', '=', 'groups.id')->orderBy('users.name')->get(['users.id']);

        /** @var array<User> */
        $users = [];

        foreach ($loggableUsers as $user) {
            $user = User::find($user->id);
            $user->profile_picture_themed = $user->getProfilePicture('white');
            unset($user->group_id);
            unset($user->created_at);
            unset($user->updated_at);
            array_push($users, $user);
        }

        return response()->json(new BaseResponseDto($users));
    }

    public function login(Request $request): JsonResponse
    {
        /** @var string */
        $username = $request->get('username');

        $user = User::where('username', $username);

        if ($request->get('password') === null) {
            $user->where('password', null);
        }

        $user = $user->first();

        if (
            $request->get('password') !== null &&
            $user !== null &&
            !Hash::check((string) $request->get('password'), (string) $user->password)
        ) {
            $user = null;
        }

        if ($user) {
            $token = JWTAuth::customClaims(['exp' => \Carbon\Carbon::now()->addHours($this->jwtHoursTTL)->timestamp])->fromUser($user);

            /** @var JsonResponse */
            return $this->createAuthTokenCookie(
                response()->json(
                    new BaseResponseDto(['ok' => true, 'token' => $token])
                ),
                $token
            );
        } else {
            return response()->json(new BaseResponseDto(['ok' => false, 'token' => null], 404, 'user-not-found'));
        }
    }

    public function logout(): RedirectResponse
    {
        return response()->redirectToRoute('login')->withoutCookie('auth-token');
    }

    public function loginPage(): \Inertia\Response
    {
        return Inertia::render('Login', ['mslAppLoginAutoSelect' => config('msl.app_login_auto-select')]);
    }

    /**
     * @param JsonResponse | \Illuminate\Http\Response $r
     * @return JsonResponse | \Illuminate\Http\Response
     * @psalm-suppress TooManyArguments
     */
    public function createAuthTokenCookie($r, string $token)
    {
        return $r->withCookie('auth-token', $token, 60 * $this->jwtHoursTTL, null, null, null, true, false, 'Lax');
    }

    public function basic(Request $request): JsonResponse
    {
        /** @var User */
        $jwt = JWTAuth::parseToken()->getPayload();

        return response()->json(
            new BaseResponseDto(
                $this->userService->basic(
                    (string) $request->get('name'),
                    (bool) $request->get('clearProfilePicture'),
                    (int) $jwt->get('user_id')
                )
            )
        );
    }

    public function uploadProfilePicture(Request $request): JsonResponse
    {
        /** @var User */
        $jwt = JWTAuth::parseToken()->getPayload();

        /** @var UploadedFile */
        $file = $request->file('profilePicture');

        return response()->json(
            new BaseResponseDto(
                $this->userService->uploadProfilePicture(
                    (int) $jwt->get('user_id'),
                    $file
                )
            )
        );
    }

    public function password(Request $request): JsonResponse
    {
        /** @var User */
        $jwt = JWTAuth::parseToken()->getPayload();

        return response()->json(
            new BaseResponseDto(
                $this->userService->changePassword(
                    (string) $request->get('currentPassword'),
                    (string) $request->get('newPassword'),
                    (string) $request->get('newPasswordRepeat'),
                    (int) $jwt->get('user_id')
                )
            )
        );
    }

    public function removePassword(Request $request): JsonResponse
    {
        /** @var User */
        $jwt = JWTAuth::parseToken()->getPayload();

        return response()->json(
            new BaseResponseDto(
                $this->userService->removePassword(
                    (string) $request->get('currentPassword'),
                    (int) $jwt->get('user_id')
                )
            )
        );
    }

}
