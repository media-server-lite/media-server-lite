<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Helpers\Filesystem\Playlist;
use App\Http\Helpers\Filesystem\Helper as FileSystemHelper;
use App\Models\Favorite;
use App\Services\FileMetadataService;
use App\Services\HistoryService;
use App\Services\ResumeService;
use Tymon\JWTAuth\Facades\JWTAuth;

class FileViewer extends Controller
{

    public function __construct(
        protected HistoryService $historyService,
        protected ResumeService $resumeService,
        protected FileMetadataService $fileMetadataService
        )
    { }

    public function view(Request $request): \Inertia\Response
    {
        /** @var string */
        $pathFromRoute = $request->route('path');

        $path = MSL_APP_MEDIA_DIRECTORY . DIRECTORY_SEPARATOR . urldecode($pathFromRoute);
        $file = new \App\Http\Helpers\Filesystem\File($path);

        $jwt = JWTAuth::parseToken()->getPayload();
        $userId = (int) $jwt->get('user_id');

        if ($file->isFile() && $file->getType() === "playlist") {
            $file = new Playlist($path);
        }

        $breadcrumb = FileSystemHelper::makeBreadcrumb(urldecode($request->path()));

        $this->historyService->increaseCounter('/' . $pathFromRoute, $userId);

        return Inertia::render(
            'FileViewer',
            [
                'file' => array_merge(
                    $file->toJSON(),
                    [
                        'breadcrumb' => $breadcrumb,
                        'favorite' => Favorite::where('user_id', $userId)->where('path', '/' . $pathFromRoute)->first() !== null,
                        'resume' => $this->resumeService->get($userId, DIRECTORY_SEPARATOR . $path),
                        'metadata' => $this->fileMetadataService->get(DIRECTORY_SEPARATOR . $path)
                    ]
                )
            ]
        );
    }
}
