<?php

namespace App\Http\Controllers;

use App\Http\CustomModels\BaseResponseDto;
use App\Services\HistoryService;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;

class HistoryController extends Controller
{

    protected HistoryService $historyService;

    public function __construct(HistoryService $historyService)
    {
        $this->historyService = $historyService;
    }

    public function clearAll(): JsonResponse
    {
        $jwt = JWTAuth::parseToken()->getPayload();

        return response()->json(
            new BaseResponseDto(
                $this->historyService->clearAll(
                    (int) $jwt->get('user_id')
                )
            )
        );
    }

    public function orderBy(string $orderBy): JsonResponse {
        /** @var User */
        $jwt = JWTAuth::parseToken()->getPayload();

        return response()->json(
            new BaseResponseDto(
                $this->historyService->getByUserIdLimit((int) $jwt->get('user_id'), 10, $orderBy)
            )
        );
    }
}
