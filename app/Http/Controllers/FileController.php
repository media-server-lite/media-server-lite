<?php

namespace App\Http\Controllers;

use App\Http\CustomModels\BaseResponseDto;
use App\Services\FavoriteService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Helpers\Filesystem\Helper as FileSystemHelper;

class FileController extends Controller
{

    protected FavoriteService $favoriteService;

    public function __construct(FavoriteService $favoriteService)
    {
        $this->favoriteService = $favoriteService;
    }

    public function favorite(Request $request): JsonResponse
    {
        $userId = (int) JWTAuth::parseToken()->getPayload()->get('user_id');

        return response()->json(new BaseResponseDto(
            $this->favoriteService->favorite(
                FileSystemHelper::normalizePath(
                    (string) $request->get('path')
                ),
                (bool) ($request->get('favorite') ?? true),
                $userId
            )
        ));
    }
}
