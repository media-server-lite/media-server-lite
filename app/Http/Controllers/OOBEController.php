<?php

namespace App\Http\Controllers;

use App\Http\CustomModels\BaseResponseDto;
use App\Services\Admin\AdminService;
use App\Services\Admin\AppService;
use App\Services\Admin\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Inertia\Inertia;

class OOBEController extends Controller
{

    protected AdminService $adminService;
    protected UserService $userService;
    protected AppService $appService;

    public function __construct(AdminService $adminService, UserService $userService, AppService $appService)
    {
        $this->adminService = $adminService;
        $this->userService = $userService;
        $this->appService = $appService;
    }

    public function view(): \Inertia\Response
    {
        return Inertia::render(
            'OOBE',
            [
                'admin' => [
                    'create' => $this->adminService->count() === 0
                ],
                'users' => [
                    'data' => $this->userService->getAll()
                ],
                'customization' => [
                    'bannedEntries' => config('msl.app_banned-entries'),
                    'loginAutoSelect' => config('msl.app_login_auto-select')
                ]
            ]
        );
    }

    public function getAllUsers(): JsonResponse
    {
        return response()->json(
            new BaseResponseDto(
                $this->userService->getAll()
            )
        );
    }

    public function createAdmin(Request $request): JsonResponse
    {
        return response()->json(
            new BaseResponseDto(
                $this->adminService->create(
                    (string) $request->get('name'),
                    (string) $request->get('username'),
                    (string) $request->get('password')
                )
            )
        );
    }

    public function createUser(Request $request): JsonResponse
    {
        /** @var string | null */
        $password = $request->get('password');
        return response()->json(
            new BaseResponseDto(
                $this->userService->create(
                    (string) $request->get('name'),
                    (string) $request->get('username'),
                    $password
                )
            )
        );
    }

    public function uploadProfilePicture(Request $request, string $username): JsonResponse
    {
        /** @var UploadedFile */
        $file = $request->file('profilePicture');

        return response()->json(
            new BaseResponseDto(
                $this->userService->uploadProfilePicture(
                    (int) $this->userService->getByUsername($username)->id,
                    $file
                )
            )
        );
    }

    public function deleteUser(string $username): JsonResponse
    {
        try {
            return response()->json(
                new BaseResponseDto(
                    $this->userService->deleteByUsername(
                        $username,
                        false
                    )
                )
            );
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(
                new BaseResponseDto(
                    [
                        'status' => 'error',
                        'error' => 'query',
                        'message' => 'Cannot delete user. They may have something related to their account. If you really want to delete this user, finish this setup and do it via the admin panel. This is a safety measure.'
                    ]
                )
            );
        }
    }

    public function saveCustomization(Request $request): JsonResponse
    {
        return response()->json(
            new BaseResponseDto(
                $this->appService->oobe(
                    (string) $request->get('appName'),
                    (string) $request->get('bannedEntries'),
                    (string) $request->get('loginAutoSelect')
                )
            )
        );
    }
}
