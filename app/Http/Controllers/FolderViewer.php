<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Filesystem\Entry;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Helpers\Filesystem\Helper as FileSystemHelper;
use App\Models\Favorite;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

class FolderViewer extends Controller
{
    public static function makeFolderData(Request $request): array
    {
        /** @var string */
        $pathFromRoute = $request->route('path');

        $path = MSL_APP_MEDIA_DIRECTORY . DIRECTORY_SEPARATOR . urldecode($pathFromRoute);

        $folder = new \App\Http\Helpers\Filesystem\Folder($path);

        $jwt = JWTAuth::parseToken()->getPayload();

        $breadcrumb = FileSystemHelper::makeBreadcrumb(FileSystemHelper::removeMediaFromPath($request->path()));

        $folder = $folder->toJSON();
        $folder['showActionBar'] = true;

        if (FacadesRequest::is('home')) {
            $folder['name'] = 'Home';
            $folder['showActionBar'] = false;
        }

        $directoryReader = new \App\Http\Helpers\DirectoryReader($path);
        $directoryReader->readDir(true, null, false);
        $folderStructure = array_map(fn (Entry $e) => FileSystemHelper::entryToPublic($e), $directoryReader->getStructure());

        return [
            'folder' => array_merge(
                $folder,
                [
                    'breadcrumb' => $breadcrumb,
                    'favorite' =>
                    Favorite::where('user_id', $jwt->get('user_id'))
                        ->where('path', '/' . $pathFromRoute)->first() !== null,
                    'content' => $folderStructure
                ]
            )
        ];
    }

    /**
     * @return \Inertia\Response | \Illuminate\Http\RedirectResponse
     */
    public function view(Request $request)
    {
        /** @var string */
        $pathFromRoute = $request->route('path');

        $path = MSL_APP_MEDIA_DIRECTORY . DIRECTORY_SEPARATOR . urldecode($pathFromRoute);

        if (!is_dir($path)) {
            return redirect()->route('file-viewer', ['path' => urldecode($pathFromRoute)]);
        }

        return Inertia::render('FolderViewer', self::makeFolderData($request));
    }
}
