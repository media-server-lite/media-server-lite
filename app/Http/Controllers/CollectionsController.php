<?php

namespace App\Http\Controllers;

use App\Exceptions\EntryNotFoundException;
use App\Http\CustomModels\BaseResponseDto;
use App\Http\Helpers\Filesystem\File;
use App\Http\Helpers\Filesystem\Folder;
use App\Services\CollectionsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Inertia\Inertia;
use App\Http\Helpers\Filesystem\Helper as FileSystemHelper;
use App\Models\CollectionPairs;

class CollectionsController extends Controller
{

    protected CollectionsService $collectionsService;

    public function __construct(CollectionsService $collectionsService)
    {
        $this->collectionsService = $collectionsService;
    }

    public function getByUserId(Request $request): JsonResponse
    {
        $jwt = JWTAuth::parseToken()->getPayload();
        /** @var string */
        $checkIfCollectionsHavePath = $request->query('path', 'false');

        $collections = $this->collectionsService->getByUserId(
            (int) $jwt->get('user_id')
        );

        if ($checkIfCollectionsHavePath !== 'false') {
            $checks = $this->collectionsService->checkIfCollectionsHavePath(
                array_map(
                    /**
                     * @return mixed
                     */
                    fn (array $c) => $c['id'],
                    $collections
                ), FileSystemHelper::normalizePath($checkIfCollectionsHavePath));

            $collections = array_map(function (array $c) use ($checks) {
                /** @var array */
                foreach ($checks as $check) {
                    if ($check['collection_id'] === $c['id']) {
                        $c['exists'] = (bool) $check['exists'];
                    }
                }

                return $c;
            }, $collections);
        }

        return response()->json(
            new BaseResponseDto(
                $collections
            )
        );
    }

    public function pair(Request $request): JsonResponse
    {
        $userId = (int) JWTAuth::parseToken()->getPayload()->get('user_id');

        return response()->json(new BaseResponseDto(
            $this->collectionsService->pair(
                (bool) $request->get('add'),
                (int) $request->get('collectionId'),
                FileSystemHelper::normalizePath(
                    (string) $request->get('path')
                ),
                $userId
            )
        ));
    }

    public function new(Request $request): JsonResponse
    {
        $userId = (int) JWTAuth::parseToken()->getPayload()->get('user_id');

        return response()->json(new BaseResponseDto(
            $this->collectionsService->new(
                (string) $request->get('name'),
                (string) $request->get('icon', null),
                $userId
            )
        ));
    }

    public function view(int $id): \Inertia\Response
    {
        $userId = (int) JWTAuth::parseToken()->getPayload()->get('user_id');
        $collection = $this->collectionsService->getById($id, $userId);

        /** @var array<int, CollectionPairs> */
        $temp = $this->collectionsService->getPairsByCollectionId($id, $userId);

        foreach ($temp as $i => &$entry) {
            $path = MSL_APP_MEDIA_DIRECTORY . DIRECTORY_SEPARATOR . (string) $entry['path'];

            try {
                $e = is_dir($path) ? new Folder($path) : new File($path);
                $entry = FileSystemHelper::entryToPublic($e);
            } catch (EntryNotFoundException $_) {
                array_splice($temp, $i, 1);
            }
        }

        return Inertia::render(
            'CollectionViewer',
            [
                'collection' => $collection,
                'content' => $temp,
                'breadcrumb' => [
                    [
                        'name' => $collection['name'],
                        'type' => 'collection'
                    ]
                ]
            ]
        );
    }

    public function rename(Request $request, int $id): JsonResponse {
        $userId = (int) JWTAuth::parseToken()->getPayload()->get('user_id');

        return response()->json(new BaseResponseDto(
            $this->collectionsService->renameCollection(
                $id,
                (string) $request->get('name'),
                $userId
            )
        ));
    }

    public function delete(int $id): JsonResponse {
        $userId = (int) JWTAuth::parseToken()->getPayload()->get('user_id');

        return response()->json(new BaseResponseDto(
            $this->collectionsService->deleteById(
                $id,
                $userId
            )
        ));
    }
}
