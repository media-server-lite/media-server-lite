<?php

namespace App\Http\Controllers;

use App\Http\CustomModels\BaseResponseDto;
use App\Services\Admin\AppService;
use Illuminate\Http\JsonResponse;

class AppController extends Controller
{

    protected AppService $appService;

    public function __construct(AppService $appService)
    {
        $this->appService = $appService;
    }

    public function info(): JsonResponse {
        return response()->json(
            new BaseResponseDto(
                [
                    'name' => config('msl.app_name'),
                    'firstRun' => $this->appService->isFirstRun()
                ]
            )
        );
    }
}
