<?php

namespace App\Http\CustomModels;

class BaseResponseDto
{
    public int $status = 200;
    public string $message = "success";

    /**
     * @var mixed
     */
    public $data;

    /**
     * @param mixed $data
     */
    public function __construct($data, int $status = null, string $message = null)
    {
        $this->data = $data;
        $this->status = $status ?? 200;
        $this->message = $message ?? "success";
    }
}
