<?php

namespace App\Http\Helpers\Filesystem;

use urvin\m3u\M3u;

class Playlist extends File
{

    protected array $structure = [];

    public function __construct(string $path)
    {
        parent::__construct($path);

        $m3u = new M3u();

        $m3u->parse(file_get_contents($path)); // parse an existing file

        $entries = $m3u->getEntries();  // walk through items
        foreach ($entries as &$entry) {
            array_push($this->structure, [
                "path" => DIRECTORY_SEPARATOR . str_replace($this->getName(true), '', $this->getPath()) . DIRECTORY_SEPARATOR . $entry->path,
                "name" => $entry->name,
                "artist" => $entry->artist,
                "length" => $entry->length,
                "group" => $entry->group
            ]);
        }
    }

    public function getStructure(): array
    {
        return $this->structure;
    }


    public function toJSON(): array
    {
        return [
            'name' => $this->getName(),
            'extension' => $this->getExtension(),
            'type' => 'file',
            'fileType' => $this->getType(),
            'path' => DIRECTORY_SEPARATOR . $this->getPath(),
            'structure' => $this->structure
        ];
    }
}
