<?php

namespace App\Http\Helpers\Filesystem;

class Folder extends Entry
{

    private static array $ICONS_PATH = ['folder.webp', 'folder.png', 'folder.jpg'];

    public function isFolder(): bool
    {
        return true;
    }
    public function toJSON(): array
    {
        return [
            'name' => $this->getName(),
            'type' => 'folder',
            'path' => DIRECTORY_SEPARATOR . $this->getPath()
        ];
    }

    public function getIcon(): ?string
    {
        /** @var string */
        foreach (self::$ICONS_PATH as $icon) {
            $path = $this->getPath() . DIRECTORY_SEPARATOR . $icon;

            if (file_exists($path)) {
                return $path;
            }
        }

        return null;
    }
}
