<?php

namespace App\Http\Helpers\Filesystem;

use App\Services\FileMetadataService;

class Helper
{
    public static function makeBreadcrumb(string $path): array
    {
        $t = explode('/', $path);
        unset($t[0]);

        $breadcrumb = [];

        foreach ($t as $i => $_) {
            $path = MSL_APP_MEDIA_DIRECTORY . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, array_slice($t, 0, $i));

            $path = urldecode($path);

            $entry = is_dir($path) ? new Folder($path) : new File($path);

            array_push($breadcrumb, self::entryToPublic($entry));
        }

        return $breadcrumb;
    }

    public static function removeIfStartsWith(string $string, string $remove): string
    {
        if (substr($string, 0, strlen($remove)) === $remove) {
            return substr($string, strlen($remove));
        }

        return $string;
    }

    public static function removeMediaFromPath(string $path, bool $slash = false): string
    {
        return self::removeIfStartsWith($path, ($slash ? '/' : '') . (string) MSL_APP_MEDIA_DIRECTORY);
    }

    public static function iconToPublic(Entry $entry): ?string
    {
        $icon = $entry->getIcon();

        return $icon ? '/' . self::windowsDirToStdDir($icon) : null;
    }

    public static function removeUselessDirectorySeparators(string $path): string
    {
        return preg_replace('#/+#', '/', $path);
    }

    public static function windowsDirToStdDir(string $path): string
    {
        return str_replace('\\', '/', $path);
    }

    public static function normalizePath(string $path): string
    {
        return self::removeMediaFromPath(
            self::removeMediaFromPath(
                self::removeUselessDirectorySeparators(
                    self::windowsDirToStdDir($path)
                ),
                true
            ),
            false
        );
    }

    public static function entryToPublic(Entry $entry): array
    {
        $metadataService = new FileMetadataService();

        $json = $entry->toJSON();

        $json['path'] = self::removeIfStartsWith(
            self::normalizePath($entry->getPath()),
            self::windowsDirToStdDir(public_path())
        );

        $json['icon'] = self::iconToPublic(
            $entry
        );

        $json['url'] = self::removeUselessDirectorySeparators(($entry->isFolder() ? "/b/" : "/f/") . $json['path']);

        $json['hasMetadata'] = $metadataService->exists(
            DIRECTORY_SEPARATOR .
            (string) MSL_APP_MEDIA_DIRECTORY .
            DIRECTORY_SEPARATOR .
            self::removeIfStartsWith(
                $json['path'], '/'
            )
        );

        return $json;
    }
}
