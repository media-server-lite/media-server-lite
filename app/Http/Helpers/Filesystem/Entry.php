<?php

namespace App\Http\Helpers\Filesystem;

use App\Exceptions\EntryNotFoundException;

abstract class Entry
{
    protected string $name;
    protected string $path;

    public function __construct(string $path)
    {
        if (!file_exists($path)) {
            throw new EntryNotFoundException($path);
        }

        $this->name = basename($path);
        $this->path = $path;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getPath(): string {
        return $this->path;
    }

    public function isFile(): bool {
        return !$this->isFolder();
    }

    public abstract function isFolder(): bool;

    public abstract function toJSON(): array;

    public abstract function getIcon(): ?string;
}
