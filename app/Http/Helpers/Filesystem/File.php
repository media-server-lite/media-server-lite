<?php

namespace App\Http\Helpers\Filesystem;

use App\Http\Helpers\DirectoryReader;

class File extends Entry
{
    protected int $size;
    protected string $extension;

    public static array $VIDEO_EXTS = ['mp4', 'avi', 'mkv', 'flv', 'webm', 'mov', 'wmv', 'm4p', 'm4v', 'mpg'];
    public static array $IMAGE_EXTS = ['jpg', 'jpeg', 'webp', 'png', 'gif', 'bmp'];
    public static array $AUDIO_EXTS = ['mp3', 'wav', 'flac', 'ogg', 'aac'];
    public static array $PLAYLIST_EXTS = ['m3u', 'm3u8'];

    public function __construct(string $path)
    {
        parent::__construct($path);

        $this->extension = \Illuminate\Support\Facades\File::extension($path);
        $this->size = filesize($path);
    }

    public function isFolder(): bool
    {
        return false;
    }

    public function toJSON(): array
    {
        return [
            'name' => $this->getName(),
            'extension' => $this->getExtension(),
            'type' => 'file',
            'fileType' => $this->getType(),
            'path' => DIRECTORY_SEPARATOR . $this->getPath(),
            'captions' => $this->getCaptions()
        ];
    }

    public function getName(bool $withExtension = false): string
    {
        return $withExtension ? $this->name : str_replace('.' . $this->extension, '', $this->name);
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function getType(): ?string
    {
        if (in_array($this->getExtension(), File::$VIDEO_EXTS)) {
            return 'video';
        } else if (in_array($this->getExtension(), File::$IMAGE_EXTS)) {
            return 'image';
        } else if (in_array($this->getExtension(), File::$AUDIO_EXTS)) {
            return 'audio';
        } else if (in_array($this->getExtension(), File::$PLAYLIST_EXTS)) {
            return 'playlist';
        }

        return null;
    }

    public function getCaptions(): ?array
    {
        $dr = new DirectoryReader(
            str_replace($this->getName(true), '', $this->getPath())
        );

        $dr->readDir(true, '/' . preg_quote($this->getName(), '~') . ' - (.*?).vtt/', true);
        $matches = $dr->getStructure();

        $captions = [];
        /** @var Entry */
        foreach ($matches as $match) {
            array_push(
                $captions,
                [
                    "path" => $match->getPath(),
                    "label" => str_replace($this->getName() . ' - ', '', $match->getName())
                ]
            );
        }

        if (count($captions) > 0) {
            return $captions;
        }

        return null;
    }

    public function getIcon(): ?string
    {
        if ($this->getType() === "image") {
            return $this->getPath();
        }

        return null;
    }
}
