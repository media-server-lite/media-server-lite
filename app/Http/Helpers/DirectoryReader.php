<?php

namespace App\Http\Helpers;

use Symfony\Component\Filesystem\Exception\IOException;

class DirectoryReader
{
    protected string $dir;
    protected array $structure = [];

    public function __construct(string $dir)
    {
        $this->dir = $dir;
    }

    public function getDir(): string
    {
        return $this->dir;
    }

    public function getStructure(): array
    {
        return $this->structure;
    }

    public function readDir(bool $folderFirst = true, string $filter = null, bool $positiveFilter = false): void
    {
        /** @var string | null */
        $filter = $filter ?? config('msl.app_banned-entries', null);

        $folders = [];
        $files = [];

        $handle = opendir($this->dir);

        if ($handle === false) {
            throw new IOException("Cannot find the path specified $this->dir");
        }

        while (false !== ($entry = readdir($handle))) {
            if ($filter === null || count(preg_grep($filter, [$entry])) === ($positiveFilter ? 1 : 0)) {
                $fullPath = $this->dir . DIRECTORY_SEPARATOR . $entry;

                $entry = is_dir($fullPath) ? new Filesystem\Folder($fullPath) : new Filesystem\File($fullPath);

                if ($folderFirst && $entry instanceof Filesystem\Folder || !$folderFirst) {
                    array_push($folders, $entry);
                } else {
                    array_push($files, $entry);
                }
            }
        }

        closedir($handle);

        $this->structure = array_merge($folders, $files);
    }
}
