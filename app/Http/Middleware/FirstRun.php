<?php

namespace App\Http\Middleware;

use App\Services\Admin\AppService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class FirstRun
{

    protected array $ALLOWED = ['welcome', '/welcome', 'api/welcome', '/api/welcome'];

    protected $appService;

    public function __construct(AppService $appService)
    {
        $this->appService = $appService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var \Illuminate\Routing\Route */
        $route = $request->route();

        $routeName = (string) $route->getName();
        $prefix = (string) (Route::current()->action['prefix'] ?? '');

        $firstRun = $this->appService->isFirstRun();

        if ($firstRun && !in_array($prefix, $this->ALLOWED)) {
            return redirect()->route('oobe');
        } else if ($firstRun === false && $routeName === 'oobe') {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
