<?php

namespace App\Http\Middleware;

use App\Http\Controllers\UserController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;
use Tymon\JWTAuth\Facades\JWTAuth;

class JWTMiddleware
{

    protected UserController $userController;

    public function __construct(UserController $userController)
    {
        $this->userController = $userController;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var string | null */
        $authToken = $request->cookie('auth-token');

        if ($authToken) {
            $request->headers->set('Authorization', "Bearer $authToken");

            try {
                JWTAuth::parseToken()->authenticate();

                $user = JWTAuth::user();

                $user['profile_picture_themed'] = $user->getProfilePicture('white');

                Inertia::share('user', $user);

                /** @var \Illuminate\Http\Response */
                $response = $next($request);

                $this->userController->createAuthTokenCookie($response, JWTAuth::fromUser(JWTAuth::user()));

                return $response;
            } catch (\Exception $e) {
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                    return redirect()->route('login', ['status' => 'Token is Invalid', 'redirect' => $request->path()]);
                } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                    return redirect()->route('login', ['status' => 'Token is Expired', 'redirect' => $request->path()]);
                }

                throw $e;
            }
        }

        return redirect()->route('login', ['status' => 'Authorization Token not found', 'redirect' => $request->path()]);
    }
}
