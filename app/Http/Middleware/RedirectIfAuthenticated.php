<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var string | null */
        $authToken = isset($_COOKIE['auth-token']) ? $_COOKIE['auth-token'] : null;

        if ($authToken) {
            $request->headers->set('Authorization', "Bearer $authToken");

            try {
                JWTAuth::parseToken()->authenticate();
                return redirect()->route('home');
            } catch (\Exception $e) {
                // User is not authenticated
            }
        }

        return $next($request);

    }
}
